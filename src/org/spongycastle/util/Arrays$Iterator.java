package org.spongycastle.util;

import java.util.Iterator;

public class Arrays$Iterator implements Iterator {
	private final Object[] dataArray;
	private int position = 0;

	public Arrays$Iterator(Object[] var1) {
		this.dataArray = var1;
	}

	public boolean hasNext() {
		return this.position < this.dataArray.length;
	}

	public Object next() {
		Object[] var1 = this.dataArray;
		int var2 = this.position++;
		return var1[var2];
	}

	public void remove() {
		throw new UnsupportedOperationException("Cannot remove element from an Array.");
	}
}
