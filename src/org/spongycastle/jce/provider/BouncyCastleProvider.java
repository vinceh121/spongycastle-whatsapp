package org.spongycastle.jce.provider;

import java.security.AccessController;
import java.security.Provider;
import java.util.HashMap;
import java.util.Map;
import org.spongycastle.jcajce.provider.config.ConfigurableProvider;
import org.spongycastle.jcajce.provider.util.AlgorithmProvider;

public final class BouncyCastleProvider extends Provider implements ConfigurableProvider {
	private static final String[] ASYMMETRIC_CIPHERS = new String[] { "DSA", "DH", "EC", "RSA", "GOST", "ECGOST",
			"ElGamal", "DSTU4145" };
	private static final String[] ASYMMETRIC_GENERIC = new String[] { "X509", "IES" };
	private static final String ASYMMETRIC_PACKAGE = "org.spongycastle.jcajce.provider.asymmetric.";
	private static final String[] DIGESTS = new String[] { "GOST3411", "Keccak", "MD2", "MD4", "MD5", "SHA1",
			"RIPEMD128", "RIPEMD160", "RIPEMD256", "RIPEMD320", "SHA224", "SHA256", "SHA384", "SHA512", "SHA3", "Skein",
			"SM3", "Tiger", "Whirlpool", "Blake2b" };
	private static final String DIGEST_PACKAGE = "org.spongycastle.jcajce.provider.digest.";
	private static final String[] KEYSTORES = new String[] { "BC", "PKCS12" };
	private static final String KEYSTORE_PACKAGE = "org.spongycastle.jcajce.provider.keystore.";
	public static final String PROVIDER_NAME = "SC";
	private static final String[] SYMMETRIC_CIPHERS = new String[] { "AES", "ARC4", "Blowfish", "Camellia", "CAST5",
			"CAST6", "ChaCha", "DES", "DESede", "GOST28147", "Grainv1", "Grain128", "HC128", "HC256", "IDEA", "Noekeon",
			"RC2", "RC5", "RC6", "Rijndael", "Salsa20", "SEED", "Serpent", "Shacal2", "Skipjack", "SM4", "TEA",
			"Twofish", "Threefish", "VMPC", "VMPCKSA3", "XTEA", "XSalsa20", "OpenSSLPBKDF" };
	private static final String[] SYMMETRIC_GENERIC = new String[] { "PBEPBKDF2", "PBEPKCS12" };
	private static final String[] SYMMETRIC_MACS = new String[] { "SipHash" };
	private static final String SYMMETRIC_PACKAGE = "org.spongycastle.jcajce.provider.symmetric.";
	private static String info = "BouncyCastle Security Provider v1.54";
	private static final Map keyInfoConverters = new HashMap();

	public BouncyCastleProvider() {
		super("SC", 1.54D, info);
		AccessController.doPrivileged(new BouncyCastleProvider$1(this));
	}

	// $FF: synthetic method
	static void access$0(BouncyCastleProvider var0) {
		var0.setup();
	}

	private void loadAlgorithms(String var1, String[] var2) {
		for (int var3 = 0; var3 != var2.length; ++var3) {
			Class var5 = null;
			label42: {
				label41: {
					boolean var10001;
					ClassLoader var7;
					try {
						var7 = this.getClass().getClassLoader();
					} catch (Exception var13) {
						var10001 = false;
						break label41;
					}

					if (var7 != null) {
						label34: {
							Class var8;
							try {
								var8 = var7.loadClass(var1 + var2[var3] + "$Mappings");
							} catch (ClassNotFoundException var11) {
								var10001 = false;
								break label34;
							}

							var5 = var8;
						}
					} else {
						label37: {
							Class var9;
							try {
								var9 = Class.forName(var1 + var2[var3] + "$Mappings");
							} catch (ClassNotFoundException var12) {
								var10001 = false;
								break label37;
							}

							var5 = var9;
						}
					}
					break label42;
				}

				var5 = null;
			}

			if (var5 != null) {
				try {
					((AlgorithmProvider) var5.newInstance()).configure(this);
				} catch (Exception var10) {
					throw new InternalError("cannot create instance of " + var1 + var2[var3] + "$Mappings : " + var10);
				}
			}
		}

	}

	private void setup() {
		this.loadAlgorithms("org.spongycastle.jcajce.provider.digest.", DIGESTS);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.symmetric.", SYMMETRIC_GENERIC);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.symmetric.", SYMMETRIC_MACS);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.symmetric.", SYMMETRIC_CIPHERS);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.asymmetric.", ASYMMETRIC_GENERIC);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.asymmetric.", ASYMMETRIC_CIPHERS);
		this.loadAlgorithms("org.spongycastle.jcajce.provider.keystore.", KEYSTORES);
		this.put("X509Store.CERTIFICATE/COLLECTION", "org.spongycastle.jce.provider.X509StoreCertCollection");
		this.put("X509Store.ATTRIBUTECERTIFICATE/COLLECTION",
				"org.spongycastle.jce.provider.X509StoreAttrCertCollection");
		this.put("X509Store.CRL/COLLECTION", "org.spongycastle.jce.provider.X509StoreCRLCollection");
		this.put("X509Store.CERTIFICATEPAIR/COLLECTION", "org.spongycastle.jce.provider.X509StoreCertPairCollection");
		this.put("X509Store.CERTIFICATE/LDAP", "org.spongycastle.jce.provider.X509StoreLDAPCerts");
		this.put("X509Store.CRL/LDAP", "org.spongycastle.jce.provider.X509StoreLDAPCRLs");
		this.put("X509Store.ATTRIBUTECERTIFICATE/LDAP", "org.spongycastle.jce.provider.X509StoreLDAPAttrCerts");
		this.put("X509Store.CERTIFICATEPAIR/LDAP", "org.spongycastle.jce.provider.X509StoreLDAPCertPairs");
		this.put("X509StreamParser.CERTIFICATE", "org.spongycastle.jce.provider.X509CertParser");
		this.put("X509StreamParser.ATTRIBUTECERTIFICATE", "org.spongycastle.jce.provider.X509AttrCertParser");
		this.put("X509StreamParser.CRL", "org.spongycastle.jce.provider.X509CRLParser");
		this.put("X509StreamParser.CERTIFICATEPAIR", "org.spongycastle.jce.provider.X509CertPairParser");
		this.put("Cipher.BROKENPBEWITHMD5ANDDES",
				"org.spongycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithMD5AndDES");
		this.put("Cipher.BROKENPBEWITHSHA1ANDDES",
				"org.spongycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithSHA1AndDES");
		this.put("Cipher.OLDPBEWITHSHAANDTWOFISH-CBC",
				"org.spongycastle.jce.provider.BrokenJCEBlockCipher$OldPBEWithSHAAndTwofish");
		this.put("CertPathValidator.RFC3281", "org.spongycastle.jce.provider.PKIXAttrCertPathValidatorSpi");
		this.put("CertPathBuilder.RFC3281", "org.spongycastle.jce.provider.PKIXAttrCertPathBuilderSpi");
		this.put("CertPathValidator.RFC3280", "org.spongycastle.jce.provider.PKIXCertPathValidatorSpi");
		this.put("CertPathBuilder.RFC3280", "org.spongycastle.jce.provider.PKIXCertPathBuilderSpi");
		this.put("CertPathValidator.PKIX", "org.spongycastle.jce.provider.PKIXCertPathValidatorSpi");
		this.put("CertPathBuilder.PKIX", "org.spongycastle.jce.provider.PKIXCertPathBuilderSpi");
		this.put("CertStore.Collection", "org.spongycastle.jce.provider.CertStoreCollectionSpi");
		this.put("CertStore.LDAP", "org.spongycastle.jce.provider.X509LDAPCertStoreSpi");
		this.put("CertStore.Multi", "org.spongycastle.jce.provider.MultiCertStoreSpi");
		this.put("Alg.Alias.CertStore.X509LDAP", "LDAP");
	}

	public void addAlgorithm(String var1, String var2) {
		if (this.containsKey(var1)) {
			throw new IllegalStateException("duplicate provider key (" + var1 + ") found");
		} else {
			this.put(var1, var2);
		}
	}

	public boolean hasAlgorithm(String var1, String var2) {
		return this.containsKey(var1 + "." + var2) || this.containsKey("Alg.Alias." + var1 + "." + var2);
	}

	public void setParameter(String var1, Object var2) {
	}
}
