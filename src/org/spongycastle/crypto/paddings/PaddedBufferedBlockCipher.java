package org.spongycastle.crypto.paddings;

import java.security.SecureRandom;
import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.BufferedBlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.OutputLengthException;
import org.spongycastle.crypto.params.ParametersWithRandom;

public class PaddedBufferedBlockCipher extends BufferedBlockCipher {
	BlockCipherPadding padding;

	public PaddedBufferedBlockCipher(BlockCipher var1) {
		this(var1, new PKCS7Padding());
	}

	public PaddedBufferedBlockCipher(BlockCipher var1, BlockCipherPadding var2) {
		this.cipher = var1;
		this.padding = var2;
		this.buf = new byte[var1.getBlockSize()];
		this.bufOff = 0;
	}

	public int doFinal(byte[] var1, int var2) throws CryptoException {
		int var3 = this.cipher.getBlockSize();
		if (this.forEncryption) {
			int var7;
			if (this.bufOff == var3) {
				if (var2 + var3 * 2 > var1.length) {
					this.reset();
					throw new OutputLengthException("output buffer too short");
				}

				var7 = this.cipher.processBlock(this.buf, 0, var1, var2);
				this.bufOff = 0;
			} else {
				var7 = 0;
			}

			this.padding.addPadding(this.buf, this.bufOff);
			int var9 = var7 + this.cipher.processBlock(this.buf, 0, var1, var2 + var7);
			this.reset();
			return var9;
		} else if (this.bufOff == var3) {
			int var4 = this.cipher.processBlock(this.buf, 0, this.buf, 0);
			this.bufOff = 0;

			int var6;
			try {
				var6 = var4 - this.padding.padCount(this.buf);
				System.arraycopy(this.buf, 0, var1, var2, var6);
			} finally {
				this.reset();
			}

			return var6;
		} else {
			this.reset();
			throw new DataLengthException("last block incomplete in decryption");
		}
	}

	public int getOutputSize(int var1) {
		int var2 = var1 + this.bufOff;
		int var3 = var2 % this.buf.length;
		if (var3 == 0) {
			if (this.forEncryption) {
				var2 += this.buf.length;
			}

			return var2;
		} else {
			return var2 - var3 + this.buf.length;
		}
	}

	public int getUpdateOutputSize(int var1) {
		int var2 = var1 + this.bufOff;
		int var3 = var2 % this.buf.length;
		return var3 == 0 ? Math.max(0, var2 - this.buf.length) : var2 - var3;
	}

	public void init(boolean var1, CipherParameters var2) {
		this.forEncryption = var1;
		this.reset();
		if (var2 instanceof ParametersWithRandom) {
			ParametersWithRandom var3 = (ParametersWithRandom) var2;
			this.padding.init(var3.getRandom());
			this.cipher.init(var1, var3.getParameters());
		} else {
			this.padding.init((SecureRandom) null);
			this.cipher.init(var1, var2);
		}
	}

	public int processByte(byte var1, byte[] var2, int var3) {
		int var4;
		if (this.bufOff == this.buf.length) {
			var4 = this.cipher.processBlock(this.buf, 0, var2, var3);
			this.bufOff = 0;
		} else {
			var4 = 0;
		}

		byte[] var5 = this.buf;
		int var6 = this.bufOff++;
		var5[var6] = var1;
		return var4;
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		if (var3 < 0) {
			throw new IllegalArgumentException("Can't have a negative input length!");
		} else {
			int var6 = this.getBlockSize();
			int var7 = this.getUpdateOutputSize(var3);
			if (var7 > 0 && var7 + var5 > var4.length) {
				throw new OutputLengthException("output buffer too short");
			} else {
				int var8 = this.buf.length - this.bufOff;
				int var9;
				int var10;
				int var11;
				if (var3 > var8) {
					System.arraycopy(var1, var2, this.buf, this.bufOff, var8);
					var11 = 0 + this.cipher.processBlock(this.buf, 0, var4, var5);
					this.bufOff = 0;
					var10 = var3 - var8;

					for (var9 = var8 + var2; var10 > this.buf.length; var9 += var6) {
						var11 += this.cipher.processBlock(var1, var9, var4, var5 + var11);
						var10 -= var6;
					}
				} else {
					var9 = var2;
					var10 = var3;
					var11 = 0;
				}

				System.arraycopy(var1, var9, this.buf, this.bufOff, var10);
				this.bufOff += var10;
				return var11;
			}
		}
	}
}
