package org.spongycastle.crypto.modes.gcm;

import java.lang.reflect.Array;
import org.spongycastle.util.Arrays;
import org.spongycastle.util.Pack;

public class Tables8kGCMMultiplier implements GCMMultiplier {
	private byte[] H;
	private int[][][] M;

	public void init(byte[] var1) {
		if (this.M == null) {
			int[] var2 = new int[] { 32, 16, 4 };
			this.M = (int[][][]) Array.newInstance(Integer.TYPE, var2);
		} else if (Arrays.areEqual(this.H, var1)) {
			return;
		}

		this.H = Arrays.clone(var1);
		GCMUtil.asInts(var1, this.M[1][8]);

		for (int var3 = 4; var3 >= 1; var3 >>= 1) {
			GCMUtil.multiplyP(this.M[1][var3 + var3], this.M[1][var3]);
		}

		GCMUtil.multiplyP(this.M[1][1], this.M[0][8]);

		for (int var4 = 4; var4 >= 1; var4 >>= 1) {
			GCMUtil.multiplyP(this.M[0][var4 + var4], this.M[0][var4]);
		}

		int var5 = 0;

		while (true) {
			do {
				for (int var6 = 2; var6 < 16; var6 += var6) {
					for (int var7 = 1; var7 < var6; ++var7) {
						GCMUtil.xor(this.M[var5][var6], this.M[var5][var7], this.M[var5][var6 + var7]);
					}
				}

				++var5;
				if (var5 == 32) {
					return;
				}
			} while (var5 <= 1);

			for (int var8 = 8; var8 > 0; var8 >>= 1) {
				GCMUtil.multiplyP8(this.M[var5 - 2][var8], this.M[var5][var8]);
			}
		}
	}

	public void multiplyH(byte[] var1) {
		int[] var2 = new int[4];

		for (int var3 = 15; var3 >= 0; --var3) {
			int[] var4 = this.M[var3 + var3][15 & var1[var3]];
			var2[0] ^= var4[0];
			var2[1] ^= var4[1];
			var2[2] ^= var4[2];
			var2[3] ^= var4[3];
			int[] var5 = this.M[1 + var3 + var3][(240 & var1[var3]) >>> 4];
			var2[0] ^= var5[0];
			var2[1] ^= var5[1];
			var2[2] ^= var5[2];
			var2[3] ^= var5[3];
		}

		Pack.intToBigEndian(var2, var1, 0);
	}
}
