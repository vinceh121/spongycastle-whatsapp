package org.spongycastle.crypto.modes.gcm;

public interface GCMExponentiator {
	void exponentiateX(long var1, byte[] var3);

	void init(byte[] var1);
}
