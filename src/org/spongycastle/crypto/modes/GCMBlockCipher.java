package org.spongycastle.crypto.modes;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.InvalidCipherTextException;
import org.spongycastle.crypto.OutputLengthException;
import org.spongycastle.crypto.modes.gcm.GCMExponentiator;
import org.spongycastle.crypto.modes.gcm.GCMMultiplier;
import org.spongycastle.crypto.modes.gcm.GCMUtil;
import org.spongycastle.crypto.modes.gcm.Tables1kGCMExponentiator;
import org.spongycastle.crypto.modes.gcm.Tables8kGCMMultiplier;
import org.spongycastle.crypto.params.AEADParameters;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.util.Arrays;
import org.spongycastle.util.Pack;

public class GCMBlockCipher implements AEADBlockCipher {
	private static final int BLOCK_SIZE = 16;
	private byte[] H;
	private byte[] J0;
	private byte[] S;
	private byte[] S_at;
	private byte[] S_atPre;
	private byte[] atBlock;
	private int atBlockPos;
	private long atLength;
	private long atLengthPre;
	private byte[] bufBlock;
	private int bufOff;
	private BlockCipher cipher;
	private byte[] counter;
	private GCMExponentiator exp;
	private boolean forEncryption;
	private byte[] initialAssociatedText;
	private byte[] macBlock;
	private int macSize;
	private GCMMultiplier multiplier;
	private byte[] nonce;
	private long totalLength;

	public GCMBlockCipher(BlockCipher var1) {
		this(var1, (GCMMultiplier) null);
	}

	public GCMBlockCipher(BlockCipher var1, GCMMultiplier var2) {
		if (var1.getBlockSize() != 16) {
			throw new IllegalArgumentException("cipher required with a block size of 16.");
		} else {
			if (var2 == null) {
				var2 = new Tables8kGCMMultiplier();
			}

			this.cipher = var1;
			this.multiplier = (GCMMultiplier) var2;
		}
	}

	private void gCTRBlock(byte[] var1, byte[] var2, int var3) {
		byte[] var4 = this.getNextCounterBlock();
		GCMUtil.xor(var4, var1);
		System.arraycopy(var4, 0, var2, var3, 16);
		byte[] var5 = this.S;
		if (this.forEncryption) {
			var1 = var4;
		}

		this.gHASHBlock(var5, var1);
		this.totalLength += 16L;
	}

	private void gCTRPartial(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		byte[] var6 = this.getNextCounterBlock();
		GCMUtil.xor(var6, var1, var2, var3);
		System.arraycopy(var6, 0, var4, var5, var3);
		byte[] var7 = this.S;
		if (this.forEncryption) {
			var1 = var6;
		}

		this.gHASHPartial(var7, var1, 0, var3);
		this.totalLength += (long) var3;
	}

	private void gHASH(byte[] var1, byte[] var2, int var3) {
		for (int var4 = 0; var4 < var3; var4 += 16) {
			this.gHASHPartial(var1, var2, var4, Math.min(var3 - var4, 16));
		}

	}

	private void gHASHBlock(byte[] var1, byte[] var2) {
		GCMUtil.xor(var1, var2);
		this.multiplier.multiplyH(var1);
	}

	private void gHASHPartial(byte[] var1, byte[] var2, int var3, int var4) {
		GCMUtil.xor(var1, var2, var3, var4);
		this.multiplier.multiplyH(var1);
	}

	private byte[] getNextCounterBlock() {
		int var1 = 1 + (255 & this.counter[15]);
		this.counter[15] = (byte) var1;
		int var2 = (var1 >>> 8) + (255 & this.counter[14]);
		this.counter[14] = (byte) var2;
		int var3 = (var2 >>> 8) + (255 & this.counter[13]);
		this.counter[13] = (byte) var3;
		int var4 = (var3 >>> 8) + (255 & this.counter[12]);
		this.counter[12] = (byte) var4;
		byte[] var5 = new byte[16];
		this.cipher.processBlock(this.counter, 0, var5, 0);
		return var5;
	}

	private void initCipher() {
		if (this.atLength > 0L) {
			System.arraycopy(this.S_at, 0, this.S_atPre, 0, 16);
			this.atLengthPre = this.atLength;
		}

		if (this.atBlockPos > 0) {
			this.gHASHPartial(this.S_atPre, this.atBlock, 0, this.atBlockPos);
			this.atLengthPre += (long) this.atBlockPos;
		}

		if (this.atLengthPre > 0L) {
			System.arraycopy(this.S_atPre, 0, this.S, 0, 16);
		}

	}

	private void outputBlock(byte[] var1, int var2) {
		if (var1.length < var2 + 16) {
			throw new OutputLengthException("Output buffer too short");
		} else {
			if (this.totalLength == 0L) {
				this.initCipher();
			}

			this.gCTRBlock(this.bufBlock, var1, var2);
			if (this.forEncryption) {
				this.bufOff = 0;
			} else {
				System.arraycopy(this.bufBlock, 16, this.bufBlock, 0, this.macSize);
				this.bufOff = this.macSize;
			}
		}
	}

	private void reset(boolean var1) {
		this.cipher.reset();
		this.S = new byte[16];
		this.S_at = new byte[16];
		this.S_atPre = new byte[16];
		this.atBlock = new byte[16];
		this.atBlockPos = 0;
		this.atLength = 0L;
		this.atLengthPre = 0L;
		this.counter = Arrays.clone(this.J0);
		this.bufOff = 0;
		this.totalLength = 0L;
		if (this.bufBlock != null) {
			Arrays.fill((byte[]) this.bufBlock, (byte) 0);
		}

		if (var1) {
			this.macBlock = null;
		}

		if (this.initialAssociatedText != null) {
			this.processAADBytes(this.initialAssociatedText, 0, this.initialAssociatedText.length);
		}

	}

	@Override
	public int doFinal(byte[] var1, int var2) throws CryptoException {
		if (this.totalLength == 0L) {
			this.initCipher();
		}

		int var3 = this.bufOff;
		if (this.forEncryption) {
			if (var1.length < var2 + var3 + this.macSize) {
				throw new OutputLengthException("Output buffer too short");
			}
		} else {
			if (var3 < this.macSize) {
				throw new InvalidCipherTextException("data too short");
			}

			var3 -= this.macSize;
			if (var1.length < var2 + var3) {
				throw new OutputLengthException("Output buffer too short");
			}
		}

		if (var3 > 0) {
			this.gCTRPartial(this.bufBlock, 0, var3, var1, var2);
		}

		this.atLength += (long) this.atBlockPos;
		if (this.atLength > this.atLengthPre) {
			if (this.atBlockPos > 0) {
				this.gHASHPartial(this.S_at, this.atBlock, 0, this.atBlockPos);
			}

			if (this.atLengthPre > 0L) {
				GCMUtil.xor(this.S_at, this.S_atPre);
			}

			long var8 = 127L + 8L * this.totalLength >>> 7;
			byte[] var10 = new byte[16];
			if (this.exp == null) {
				this.exp = new Tables1kGCMExponentiator();
				this.exp.init(this.H);
			}

			this.exp.exponentiateX(var8, var10);
			GCMUtil.multiply(this.S_at, var10);
			GCMUtil.xor(this.S, this.S_at);
		}

		byte[] var4 = new byte[16];
		Pack.longToBigEndian(8L * this.atLength, var4, 0);
		Pack.longToBigEndian(8L * this.totalLength, var4, 8);
		this.gHASHBlock(this.S, var4);
		byte[] var5 = new byte[16];
		this.cipher.processBlock(this.J0, 0, var5, 0);
		GCMUtil.xor(var5, this.S);
		this.macBlock = new byte[this.macSize];
		System.arraycopy(var5, 0, this.macBlock, 0, this.macSize);
		if (this.forEncryption) {
			System.arraycopy(this.macBlock, 0, var1, var2 + this.bufOff, this.macSize);
			var3 += this.macSize;
		} else {
			byte[] var7 = new byte[this.macSize];
			System.arraycopy(this.bufBlock, var3, var7, 0, this.macSize);
			if (!Arrays.constantTimeAreEqual(this.macBlock, var7)) {
				throw new InvalidCipherTextException("mac check in GCM failed");
			}
		}

		this.reset(false);
		return var3;
	}

	public String getAlgorithmName() {
		return this.cipher.getAlgorithmName() + "/GCM";
	}

	public byte[] getMac() {
		return Arrays.clone(this.macBlock);
	}

	public int getOutputSize(int var1) {
		int var2 = var1 + this.bufOff;
		if (this.forEncryption) {
			return var2 + this.macSize;
		} else {
			return var2 < this.macSize ? 0 : var2 - this.macSize;
		}
	}

	public BlockCipher getUnderlyingCipher() {
		return this.cipher;
	}

	public int getUpdateOutputSize(int var1) {
		int var2 = var1 + this.bufOff;
		if (!this.forEncryption) {
			if (var2 < this.macSize) {
				return 0;
			}

			var2 -= this.macSize;
		}

		return var2 - var2 % 16;
	}

	public void init(boolean var1, CipherParameters var2) {
		this.forEncryption = var1;
		this.macBlock = null;
		KeyParameter var4;
		if (var2 instanceof AEADParameters) {
			AEADParameters var8 = (AEADParameters) var2;
			this.nonce = var8.getNonce();
			this.initialAssociatedText = var8.getAssociatedText();
			int var9 = var8.getMacSize();
			if (var9 < 32 || var9 > 128 || var9 % 8 != 0) {
				throw new IllegalArgumentException("Invalid value for MAC size: " + var9);
			}

			this.macSize = var9 / 8;
			var4 = var8.getKey();
		} else {
			if (!(var2 instanceof ParametersWithIV)) {
				throw new IllegalArgumentException("invalid parameters passed to GCM");
			}

			ParametersWithIV var3 = (ParametersWithIV) var2;
			this.nonce = var3.getIV();
			this.initialAssociatedText = null;
			this.macSize = 16;
			var4 = (KeyParameter) var3.getParameters();
		}

		int var5;
		if (var1) {
			var5 = 16;
		} else {
			var5 = 16 + this.macSize;
		}

		this.bufBlock = new byte[var5];
		if (this.nonce != null && this.nonce.length >= 1) {
			if (var4 != null) {
				this.cipher.init(true, var4);
				this.H = new byte[16];
				this.cipher.processBlock(this.H, 0, this.H, 0);
				this.multiplier.init(this.H);
				this.exp = null;
			} else if (this.H == null) {
				throw new IllegalArgumentException("Key must be specified in initial init");
			}

			this.J0 = new byte[16];
			if (this.nonce.length == 12) {
				System.arraycopy(this.nonce, 0, this.J0, 0, this.nonce.length);
				this.J0[15] = 1;
			} else {
				this.gHASH(this.J0, this.nonce, this.nonce.length);
				byte[] var6 = new byte[16];
				Pack.longToBigEndian(8L * (long) this.nonce.length, var6, 8);
				this.gHASHBlock(this.J0, var6);
			}

			this.S = new byte[16];
			this.S_at = new byte[16];
			this.S_atPre = new byte[16];
			this.atBlock = new byte[16];
			this.atBlockPos = 0;
			this.atLength = 0L;
			this.atLengthPre = 0L;
			this.counter = Arrays.clone(this.J0);
			this.bufOff = 0;
			this.totalLength = 0L;
			if (this.initialAssociatedText != null) {
				this.processAADBytes(this.initialAssociatedText, 0, this.initialAssociatedText.length);
			}

		} else {
			throw new IllegalArgumentException("IV must be at least 1 byte");
		}
	}

	public void processAADByte(byte var1) {
		this.atBlock[this.atBlockPos] = var1;
		int var2 = 1 + this.atBlockPos;
		this.atBlockPos = var2;
		if (var2 == 16) {
			this.gHASHBlock(this.S_at, this.atBlock);
			this.atBlockPos = 0;
			this.atLength += 16L;
		}

	}

	public void processAADBytes(byte[] var1, int var2, int var3) {
		for (int var4 = 0; var4 < var3; ++var4) {
			this.atBlock[this.atBlockPos] = var1[var2 + var4];
			int var5 = 1 + this.atBlockPos;
			this.atBlockPos = var5;
			if (var5 == 16) {
				this.gHASHBlock(this.S_at, this.atBlock);
				this.atBlockPos = 0;
				this.atLength += 16L;
			}
		}

	}

	public int processByte(byte var1, byte[] var2, int var3) {
		this.bufBlock[this.bufOff] = var1;
		int var4 = 1 + this.bufOff;
		this.bufOff = var4;
		if (var4 == this.bufBlock.length) {
			this.outputBlock(var2, var3);
			return 16;
		} else {
			return 0;
		}
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		int var6 = 0;
		if (var1.length < var2 + var3) {
			throw new DataLengthException("Input buffer too short");
		} else {
			for (int var7 = 0; var7 < var3; ++var7) {
				this.bufBlock[this.bufOff] = var1[var2 + var7];
				int var8 = 1 + this.bufOff;
				this.bufOff = var8;
				if (var8 == this.bufBlock.length) {
					this.outputBlock(var4, var5 + var6);
					var6 += 16;
				}
			}

			return var6;
		}
	}

	public void reset() {
		this.reset(true);
	}
}
