package org.spongycastle.crypto.modes;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.StreamBlockCipher;
import org.spongycastle.crypto.params.ParametersWithIV;

public class OFBBlockCipher extends StreamBlockCipher {
	private byte[] IV;
	private final int blockSize;
	private int byteCount;
	private final BlockCipher cipher;
	private byte[] ofbOutV;
	private byte[] ofbV;

	public OFBBlockCipher(BlockCipher var1, int var2) {
		super(var1);
		this.cipher = var1;
		this.blockSize = var2 / 8;
		this.IV = new byte[var1.getBlockSize()];
		this.ofbV = new byte[var1.getBlockSize()];
		this.ofbOutV = new byte[var1.getBlockSize()];
	}

	protected byte calculateByte(byte var1) {
		if (this.byteCount == 0) {
			this.cipher.processBlock(this.ofbV, 0, this.ofbOutV, 0);
		}

		byte[] var2 = this.ofbOutV;
		int var3 = this.byteCount++;
		byte var4 = (byte) (var1 ^ var2[var3]);
		if (this.byteCount == this.blockSize) {
			this.byteCount = 0;
			System.arraycopy(this.ofbV, this.blockSize, this.ofbV, 0, this.ofbV.length - this.blockSize);
			System.arraycopy(this.ofbOutV, 0, this.ofbV, this.ofbV.length - this.blockSize, this.blockSize);
		}

		return var4;
	}

	public String getAlgorithmName() {
		return this.cipher.getAlgorithmName() + "/OFB" + 8 * this.blockSize;
	}

	public int getBlockSize() {
		return this.blockSize;
	}

	public void init(boolean var1, CipherParameters var2) {
		if (var2 instanceof ParametersWithIV) {
			ParametersWithIV var3 = (ParametersWithIV) var2;
			byte[] var4 = var3.getIV();
			if (var4.length < this.IV.length) {
				System.arraycopy(var4, 0, this.IV, this.IV.length - var4.length, var4.length);

				for (int var5 = 0; var5 < this.IV.length - var4.length; ++var5) {
					this.IV[var5] = 0;
				}
			} else {
				System.arraycopy(var4, 0, this.IV, 0, this.IV.length);
			}

			this.reset();
			if (var3.getParameters() != null) {
				this.cipher.init(true, var3.getParameters());
			}
		} else {
			this.reset();
			if (var2 != null) {
				this.cipher.init(true, var2);
				return;
			}
		}

	}

	public int processBlock(byte[] var1, int var2, byte[] var3, int var4) {
		this.processBytes(var1, var2, this.blockSize, var3, var4);
		return this.blockSize;
	}

	public void reset() {
		System.arraycopy(this.IV, 0, this.ofbV, 0, this.IV.length);
		this.byteCount = 0;
		this.cipher.reset();
	}
}
