package org.spongycastle.crypto.modes;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.util.Arrays;

public class CBCBlockCipher implements BlockCipher {
	private byte[] IV;
	private int blockSize;
	private byte[] cbcNextV;
	private byte[] cbcV;
	private BlockCipher cipher = null;
	private boolean encrypting;

	public CBCBlockCipher(BlockCipher var1) {
		this.cipher = var1;
		this.blockSize = var1.getBlockSize();
		this.IV = new byte[this.blockSize];
		this.cbcV = new byte[this.blockSize];
		this.cbcNextV = new byte[this.blockSize];
	}

	private int decryptBlock(byte[] var1, int var2, byte[] var3, int var4) {
		int var5 = 0;
		if (var2 + this.blockSize > var1.length) {
			throw new DataLengthException("input buffer too short");
		} else {
			System.arraycopy(var1, var2, this.cbcNextV, 0, this.blockSize);

			int var6;
			for (var6 = this.cipher.processBlock(var1, var2, var3, var4); var5 < this.blockSize; ++var5) {
				int var7 = var4 + var5;
				var3[var7] ^= this.cbcV[var5];
			}

			byte[] var8 = this.cbcV;
			this.cbcV = this.cbcNextV;
			this.cbcNextV = var8;
			return var6;
		}
	}

	private int encryptBlock(byte[] var1, int var2, byte[] var3, int var4) {
		if (var2 + this.blockSize > var1.length) {
			throw new DataLengthException("input buffer too short");
		} else {
			for (int var5 = 0; var5 < this.blockSize; ++var5) {
				byte[] var6 = this.cbcV;
				var6[var5] ^= var1[var2 + var5];
			}

			int var7 = this.cipher.processBlock(this.cbcV, 0, var3, var4);
			System.arraycopy(var3, var4, this.cbcV, 0, this.cbcV.length);
			return var7;
		}
	}

	public String getAlgorithmName() {
		return this.cipher.getAlgorithmName() + "/CBC";
	}

	public int getBlockSize() {
		return this.cipher.getBlockSize();
	}

	public BlockCipher getUnderlyingCipher() {
		return this.cipher;
	}

	public void init(boolean var1, CipherParameters var2) {
		boolean var3 = this.encrypting;
		this.encrypting = var1;
		if (var2 instanceof ParametersWithIV) {
			ParametersWithIV var4 = (ParametersWithIV) var2;
			byte[] var5 = var4.getIV();
			if (var5.length != this.blockSize) {
				throw new IllegalArgumentException("initialisation vector must be the same length as block size");
			}

			System.arraycopy(var5, 0, this.IV, 0, var5.length);
			this.reset();
			if (var4.getParameters() != null) {
				this.cipher.init(var1, var4.getParameters());
			} else if (var3 != var1) {
				throw new IllegalArgumentException("cannot change encrypting state without providing key.");
			}
		} else {
			this.reset();
			if (var2 != null) {
				this.cipher.init(var1, var2);
				return;
			}

			if (var3 != var1) {
				throw new IllegalArgumentException("cannot change encrypting state without providing key.");
			}
		}

	}

	public int processBlock(byte[] var1, int var2, byte[] var3, int var4) {
		return this.encrypting ? this.encryptBlock(var1, var2, var3, var4) : this.decryptBlock(var1, var2, var3, var4);
	}

	public void reset() {
		System.arraycopy(this.IV, 0, this.cbcV, 0, this.IV.length);
		Arrays.fill((byte[]) this.cbcNextV, (byte) 0);
		this.cipher.reset();
	}
}
