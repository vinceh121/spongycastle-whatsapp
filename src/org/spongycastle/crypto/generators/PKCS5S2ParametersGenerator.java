package org.spongycastle.crypto.generators;

import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.Digest;
import org.spongycastle.crypto.Mac;
import org.spongycastle.crypto.PBEParametersGenerator;
import org.spongycastle.crypto.digests.SHA1Digest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.util.Arrays;

public class PKCS5S2ParametersGenerator extends PBEParametersGenerator {
	private Mac hMac;
	private byte[] state;

	public PKCS5S2ParametersGenerator() {
		this(new SHA1Digest());
	}

	public PKCS5S2ParametersGenerator(Digest var1) {
		this.hMac = new HMac(var1);
		this.state = new byte[this.hMac.getMacSize()];
	}

	private void F(byte[] var1, int var2, byte[] var3, byte[] var4, int var5) {
		if (var2 == 0) {
			throw new IllegalArgumentException("iteration count must be at least 1.");
		} else {
			if (var1 != null) {
				this.hMac.update(var1, 0, var1.length);
			}

			this.hMac.update(var3, 0, var3.length);
			this.hMac.doFinal(this.state, 0);
			System.arraycopy(this.state, 0, var4, var5, this.state.length);

			for (int var7 = 1; var7 < var2; ++var7) {
				this.hMac.update(this.state, 0, this.state.length);
				this.hMac.doFinal(this.state, 0);

				for (int var9 = 0; var9 != this.state.length; ++var9) {
					int var10 = var5 + var9;
					var4[var10] ^= this.state[var9];
				}
			}

		}
	}

	private byte[] generateDerivedKey(int var1) {
		int var2 = this.hMac.getMacSize();
		int var3 = (-1 + var1 + var2) / var2;
		byte[] var4 = new byte[4];
		byte[] var5 = new byte[var3 * var2];
		int var6 = 0;
		KeyParameter var7 = new KeyParameter(this.password);
		this.hMac.init(var7);

		for (int var8 = 1; var8 <= var3; ++var8) {
			int var9 = 3;

			while (true) {
				byte var10 = (byte) (1 + var4[var9]);
				var4[var9] = var10;
				if (var10 != 0) {
					this.F(this.salt, this.iterationCount, var4, var5, var6);
					var6 += var2;
					break;
				}

				--var9;
			}
		}

		return var5;
	}

	public CipherParameters generateDerivedMacParameters(int var1) {
		return this.generateDerivedParameters(var1);
	}

	public CipherParameters generateDerivedParameters(int var1) {
		int var2 = var1 / 8;
		return new KeyParameter(Arrays.copyOfRange((byte[]) this.generateDerivedKey(var2), 0, var2), 0, var2);
	}

	public CipherParameters generateDerivedParameters(int var1, int var2) {
		int var3 = var1 / 8;
		int var4 = var2 / 8;
		byte[] var5 = this.generateDerivedKey(var3 + var4);
		return new ParametersWithIV(new KeyParameter(var5, 0, var3), var5, var3, var4);
	}
}
