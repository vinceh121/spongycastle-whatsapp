package org.spongycastle.crypto;

public interface Mac {
	int doFinal(byte[] var1, int var2);

	String getAlgorithmName();

	int getMacSize();

	void init(CipherParameters var1);

	void reset();

	void update(byte var1);

	void update(byte[] var1, int var2, int var3);
}
