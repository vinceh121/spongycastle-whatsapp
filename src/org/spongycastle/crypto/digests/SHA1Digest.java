package org.spongycastle.crypto.digests;

import org.spongycastle.util.Memoable;
import org.spongycastle.util.Pack;

public class SHA1Digest extends GeneralDigest implements EncodableDigest {
	private static final int DIGEST_LENGTH = 20;
	private static final int Y1 = 1518500249;
	private static final int Y2 = 1859775393;
	private static final int Y3 = -1894007588;
	private static final int Y4 = -899497514;
	private int H1;
	private int H2;
	private int H3;
	private int H4;
	private int H5;
	private int[] X = new int[80];
	private int xOff;

	public SHA1Digest() {
		this.reset();
	}

	public SHA1Digest(SHA1Digest var1) {
		super((GeneralDigest) var1);
		this.copyIn(var1);
	}

	public SHA1Digest(byte[] var1) {
		super(var1);
		this.H1 = Pack.bigEndianToInt(var1, 16);
		this.H2 = Pack.bigEndianToInt(var1, 20);
		this.H3 = Pack.bigEndianToInt(var1, 24);
		this.H4 = Pack.bigEndianToInt(var1, 28);
		this.H5 = Pack.bigEndianToInt(var1, 32);
		this.xOff = Pack.bigEndianToInt(var1, 36);

		for (int var2 = 0; var2 != this.xOff; ++var2) {
			this.X[var2] = Pack.bigEndianToInt(var1, 40 + var2 * 4);
		}

	}

	private void copyIn(SHA1Digest var1) {
		this.H1 = var1.H1;
		this.H2 = var1.H2;
		this.H3 = var1.H3;
		this.H4 = var1.H4;
		this.H5 = var1.H5;
		System.arraycopy(var1.X, 0, this.X, 0, var1.X.length);
		this.xOff = var1.xOff;
	}

	private int f(int var1, int var2, int var3) {
		return var1 & var2 | var3 & ~var1;
	}

	private int g(int var1, int var2, int var3) {
		return var1 & var2 | var1 & var3 | var2 & var3;
	}

	private int h(int var1, int var2, int var3) {
		return var3 ^ var1 ^ var2;
	}

	public Memoable copy() {
		return new SHA1Digest(this);
	}

	public int doFinal(byte[] var1, int var2) {
		this.finish();
		Pack.intToBigEndian(this.H1, var1, var2);
		Pack.intToBigEndian(this.H2, var1, var2 + 4);
		Pack.intToBigEndian(this.H3, var1, var2 + 8);
		Pack.intToBigEndian(this.H4, var1, var2 + 12);
		Pack.intToBigEndian(this.H5, var1, var2 + 16);
		this.reset();
		return 20;
	}

	public String getAlgorithmName() {
		return "SHA-1";
	}

	public int getDigestSize() {
		return 20;
	}

	public byte[] getEncodedState() {
		byte[] var1 = new byte[40 + 4 * this.xOff];
		super.populateState(var1);
		Pack.intToBigEndian(this.H1, var1, 16);
		Pack.intToBigEndian(this.H2, var1, 20);
		Pack.intToBigEndian(this.H3, var1, 24);
		Pack.intToBigEndian(this.H4, var1, 28);
		Pack.intToBigEndian(this.H5, var1, 32);
		Pack.intToBigEndian(this.xOff, var1, 36);

		for (int var2 = 0; var2 != this.xOff; ++var2) {
			Pack.intToBigEndian(this.X[var2], var1, 40 + var2 * 4);
		}

		return var1;
	}

	protected void processBlock() {
		for (int var1 = 16; var1 < 80; ++var1) {
			int var2 = this.X[var1 - 3] ^ this.X[var1 - 8] ^ this.X[var1 - 14] ^ this.X[var1 - 16];
			this.X[var1] = var2 << 1 | var2 >>> 31;
		}

		int var3 = this.H1;
		int var4 = this.H2;
		int var5 = this.H3;
		int var6 = this.H4;
		int var7 = this.H5;
		int var8 = 0;

		for (int var9 = 0; var9 < 4; ++var9) {
			int var10 = (var3 << 5 | var3 >>> 27) + this.f(var4, var5, var6);
			int[] var11 = this.X;
			int var12 = var8 + 1;
			int var13 = var7 + 1518500249 + var10 + var11[var8];
			int var14 = var4 << 30 | var4 >>> 2;
			int var15 = (var13 << 5 | var13 >>> 27) + this.f(var3, var14, var5);
			int[] var16 = this.X;
			int var17 = var12 + 1;
			int var18 = var6 + 1518500249 + var15 + var16[var12];
			int var19 = var3 << 30 | var3 >>> 2;
			int var20 = (var18 << 5 | var18 >>> 27) + this.f(var13, var19, var14);
			int[] var21 = this.X;
			int var22 = var17 + 1;
			int var23 = var5 + 1518500249 + var20 + var21[var17];
			var7 = var13 << 30 | var13 >>> 2;
			int var24 = (var23 << 5 | var23 >>> 27) + this.f(var18, var7, var19);
			int[] var25 = this.X;
			int var26 = var22 + 1;
			var4 = var14 + 1518500249 + var24 + var25[var22];
			var6 = var18 << 30 | var18 >>> 2;
			int var27 = (var4 << 5 | var4 >>> 27) + this.f(var23, var6, var7);
			int[] var28 = this.X;
			var8 = var26 + 1;
			var3 = var19 + 1518500249 + var27 + var28[var26];
			var5 = var23 << 30 | var23 >>> 2;
		}

		for (int var29 = 0; var29 < 4; ++var29) {
			int var30 = (var3 << 5 | var3 >>> 27) + this.h(var4, var5, var6);
			int[] var31 = this.X;
			int var32 = var8 + 1;
			int var33 = var7 + 1859775393 + var30 + var31[var8];
			int var34 = var4 << 30 | var4 >>> 2;
			int var35 = (var33 << 5 | var33 >>> 27) + this.h(var3, var34, var5);
			int[] var36 = this.X;
			int var37 = var32 + 1;
			int var38 = var6 + 1859775393 + var35 + var36[var32];
			int var39 = var3 << 30 | var3 >>> 2;
			int var40 = (var38 << 5 | var38 >>> 27) + this.h(var33, var39, var34);
			int[] var41 = this.X;
			int var42 = var37 + 1;
			int var43 = var5 + 1859775393 + var40 + var41[var37];
			var7 = var33 << 30 | var33 >>> 2;
			int var44 = (var43 << 5 | var43 >>> 27) + this.h(var38, var7, var39);
			int[] var45 = this.X;
			int var46 = var42 + 1;
			var4 = var34 + 1859775393 + var44 + var45[var42];
			var6 = var38 << 30 | var38 >>> 2;
			int var47 = (var4 << 5 | var4 >>> 27) + this.h(var43, var6, var7);
			int[] var48 = this.X;
			var8 = var46 + 1;
			var3 = var39 + 1859775393 + var47 + var48[var46];
			var5 = var43 << 30 | var43 >>> 2;
		}

		for (int var49 = 0; var49 < 4; ++var49) {
			int var50 = (var3 << 5 | var3 >>> 27) + this.g(var4, var5, var6);
			int[] var51 = this.X;
			int var52 = var8 + 1;
			int var53 = var7 + -1894007588 + var50 + var51[var8];
			int var54 = var4 << 30 | var4 >>> 2;
			int var55 = (var53 << 5 | var53 >>> 27) + this.g(var3, var54, var5);
			int[] var56 = this.X;
			int var57 = var52 + 1;
			int var58 = var6 + -1894007588 + var55 + var56[var52];
			int var59 = var3 << 30 | var3 >>> 2;
			int var60 = (var58 << 5 | var58 >>> 27) + this.g(var53, var59, var54);
			int[] var61 = this.X;
			int var62 = var57 + 1;
			int var63 = var5 + -1894007588 + var60 + var61[var57];
			var7 = var53 << 30 | var53 >>> 2;
			int var64 = (var63 << 5 | var63 >>> 27) + this.g(var58, var7, var59);
			int[] var65 = this.X;
			int var66 = var62 + 1;
			var4 = var54 + -1894007588 + var64 + var65[var62];
			var6 = var58 << 30 | var58 >>> 2;
			int var67 = (var4 << 5 | var4 >>> 27) + this.g(var63, var6, var7);
			int[] var68 = this.X;
			var8 = var66 + 1;
			var3 = var59 + -1894007588 + var67 + var68[var66];
			var5 = var63 << 30 | var63 >>> 2;
		}

		for (int var69 = 0; var69 <= 3; ++var69) {
			int var70 = (var3 << 5 | var3 >>> 27) + this.h(var4, var5, var6);
			int[] var71 = this.X;
			int var72 = var8 + 1;
			int var73 = var7 + -899497514 + var70 + var71[var8];
			int var74 = var4 << 30 | var4 >>> 2;
			int var75 = (var73 << 5 | var73 >>> 27) + this.h(var3, var74, var5);
			int[] var76 = this.X;
			int var77 = var72 + 1;
			int var78 = var6 + -899497514 + var75 + var76[var72];
			int var79 = var3 << 30 | var3 >>> 2;
			int var80 = (var78 << 5 | var78 >>> 27) + this.h(var73, var79, var74);
			int[] var81 = this.X;
			int var82 = var77 + 1;
			int var83 = var5 + -899497514 + var80 + var81[var77];
			var7 = var73 << 30 | var73 >>> 2;
			int var84 = (var83 << 5 | var83 >>> 27) + this.h(var78, var7, var79);
			int[] var85 = this.X;
			int var86 = var82 + 1;
			var4 = var74 + -899497514 + var84 + var85[var82];
			var6 = var78 << 30 | var78 >>> 2;
			int var87 = (var4 << 5 | var4 >>> 27) + this.h(var83, var6, var7);
			int[] var88 = this.X;
			var8 = var86 + 1;
			var3 = var79 + -899497514 + var87 + var88[var86];
			var5 = var83 << 30 | var83 >>> 2;
		}

		this.H1 += var3;
		this.H2 += var4;
		this.H3 += var5;
		this.H4 += var6;
		this.H5 += var7;
		this.xOff = 0;

		for (int var89 = 0; var89 < 16; ++var89) {
			this.X[var89] = 0;
		}

	}

	protected void processLength(long var1) {
		if (this.xOff > 14) {
			this.processBlock();
		}

		this.X[14] = (int) (var1 >>> 32);
		this.X[15] = (int) (-1L & var1);
	}

	protected void processWord(byte[] var1, int var2) {
		int var3 = var1[var2] << 24;
		int var4 = var2 + 1;
		int var5 = var3 | (255 & var1[var4]) << 16;
		int var6 = var4 + 1;
		int var7 = var5 | (255 & var1[var6]) << 8 | 255 & var1[var6 + 1];
		this.X[this.xOff] = var7;
		int var8 = 1 + this.xOff;
		this.xOff = var8;
		if (var8 == 16) {
			this.processBlock();
		}

	}

	public void reset() {
		super.reset();
		this.H1 = 1732584193;
		this.H2 = -271733879;
		this.H3 = -1732584194;
		this.H4 = 271733878;
		this.H5 = -1009589776;
		this.xOff = 0;

		for (int var1 = 0; var1 != this.X.length; ++var1) {
			this.X[var1] = 0;
		}

	}

	public void reset(Memoable var1) {
		SHA1Digest var2 = (SHA1Digest) var1;
		super.copyIn(var2);
		this.copyIn(var2);
	}
}
