package org.spongycastle.crypto.digests;

import org.spongycastle.util.Memoable;
import org.spongycastle.util.MemoableResetException;
import org.spongycastle.util.Pack;

public class SHA512tDigest extends LongDigest {
	private long H1t;
	private long H2t;
	private long H3t;
	private long H4t;
	private long H5t;
	private long H6t;
	private long H7t;
	private long H8t;
	private int digestLength;

	public SHA512tDigest(int var1) {
		if (var1 >= 512) {
			throw new IllegalArgumentException("bitLength cannot be >= 512");
		} else if (var1 % 8 != 0) {
			throw new IllegalArgumentException("bitLength needs to be a multiple of 8");
		} else if (var1 == 384) {
			throw new IllegalArgumentException("bitLength cannot be 384 use SHA384 instead");
		} else {
			this.digestLength = var1 / 8;
			this.tIvGenerate(8 * this.digestLength);
			this.reset();
		}
	}

	public SHA512tDigest(SHA512tDigest var1) {
		super(var1);
		this.digestLength = var1.digestLength;
		this.reset(var1);
	}

	public SHA512tDigest(byte[] var1) {
		this(readDigestLength(var1));
		this.restoreState(var1);
	}

	private static void intToBigEndian(int var0, byte[] var1, int var2, int var3) {
		int var4 = Math.min(4, var3);

		while (true) {
			--var4;
			if (var4 < 0) {
				return;
			}

			int var5 = 8 * (3 - var4);
			var1[var2 + var4] = (byte) (var0 >>> var5);
		}
	}

	private static void longToBigEndian(long var0, byte[] var2, int var3, int var4) {
		if (var4 > 0) {
			intToBigEndian((int) (var0 >>> 32), var2, var3, var4);
			if (var4 > 4) {
				intToBigEndian((int) (4294967295L & var0), var2, var3 + 4, var4 - 4);
			}
		}

	}

	private static int readDigestLength(byte[] var0) {
		return Pack.bigEndianToInt(var0, -4 + var0.length);
	}

	private void tIvGenerate(int var1) {
		this.H1 = -3482333909917012819L;
		this.H2 = 2216346199247487646L;
		this.H3 = -7364697282686394994L;
		this.H4 = 65953792586715988L;
		this.H5 = -816286391624063116L;
		this.H6 = 4512832404995164602L;
		this.H7 = -5033199132376557362L;
		this.H8 = -124578254951840548L;
		this.update((byte) 83);
		this.update((byte) 72);
		this.update((byte) 65);
		this.update((byte) 45);
		this.update((byte) 53);
		this.update((byte) 49);
		this.update((byte) 50);
		this.update((byte) 47);
		if (var1 > 100) {
			this.update((byte) (48 + var1 / 100));
			int var2 = var1 % 100;
			this.update((byte) (48 + var2 / 10));
			this.update((byte) (48 + var2 % 10));
		} else if (var1 > 10) {
			this.update((byte) (48 + var1 / 10));
			this.update((byte) (48 + var1 % 10));
		} else {
			this.update((byte) (var1 + 48));
		}

		this.finish();
		this.H1t = this.H1;
		this.H2t = this.H2;
		this.H3t = this.H3;
		this.H4t = this.H4;
		this.H5t = this.H5;
		this.H6t = this.H6;
		this.H7t = this.H7;
		this.H8t = this.H8;
	}

	public Memoable copy() {
		return new SHA512tDigest(this);
	}

	public int doFinal(byte[] var1, int var2) {
		this.finish();
		longToBigEndian(this.H1, var1, var2, this.digestLength);
		longToBigEndian(this.H2, var1, var2 + 8, -8 + this.digestLength);
		longToBigEndian(this.H3, var1, var2 + 16, -16 + this.digestLength);
		longToBigEndian(this.H4, var1, var2 + 24, -24 + this.digestLength);
		longToBigEndian(this.H5, var1, var2 + 32, -32 + this.digestLength);
		longToBigEndian(this.H6, var1, var2 + 40, -40 + this.digestLength);
		longToBigEndian(this.H7, var1, var2 + 48, -48 + this.digestLength);
		longToBigEndian(this.H8, var1, var2 + 56, -56 + this.digestLength);
		this.reset();
		return this.digestLength;
	}

	public String getAlgorithmName() {
		return "SHA-512/" + Integer.toString(8 * this.digestLength);
	}

	public int getDigestSize() {
		return this.digestLength;
	}

	public byte[] getEncodedState() {
		int var1 = this.getEncodedStateSize();
		byte[] var2 = new byte[var1 + 4];
		this.populateState(var2);
		Pack.intToBigEndian(8 * this.digestLength, var2, var1);
		return var2;
	}

	public void reset() {
		super.reset();
		this.H1 = this.H1t;
		this.H2 = this.H2t;
		this.H3 = this.H3t;
		this.H4 = this.H4t;
		this.H5 = this.H5t;
		this.H6 = this.H6t;
		this.H7 = this.H7t;
		this.H8 = this.H8t;
	}

	public void reset(Memoable var1) {
		SHA512tDigest var2 = (SHA512tDigest) var1;
		if (this.digestLength != var2.digestLength) {
			throw new MemoableResetException("digestLength inappropriate in other");
		} else {
			super.copyIn(var2);
			this.H1t = var2.H1t;
			this.H2t = var2.H2t;
			this.H3t = var2.H3t;
			this.H4t = var2.H4t;
			this.H5t = var2.H5t;
			this.H6t = var2.H6t;
			this.H7t = var2.H7t;
			this.H8t = var2.H8t;
		}
	}
}
