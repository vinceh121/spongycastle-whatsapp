package org.spongycastle.crypto.digests;

import org.spongycastle.crypto.ExtendedDigest;
import org.spongycastle.util.Arrays;

public class KeccakDigest implements ExtendedDigest {
	private static int[] KeccakRhoOffsets = keccakInitializeRhoOffsets();
	private static long[] KeccakRoundConstants = keccakInitializeRoundConstants();
	long[] C;
	protected int bitsAvailableForSqueezing;
	protected int bitsInQueue;
	long[] chiC;
	protected byte[] chunk;
	protected byte[] dataQueue;
	protected int fixedOutputLength;
	protected byte[] oneByte;
	protected int rate;
	protected boolean squeezing;
	protected byte[] state;
	long[] tempA;

	public KeccakDigest() {
		this(288);
	}

	public KeccakDigest(int var1) {
		this.state = new byte[200];
		this.dataQueue = new byte[192];
		this.C = new long[5];
		this.tempA = new long[25];
		this.chiC = new long[5];
		this.init(var1);
	}

	public KeccakDigest(KeccakDigest var1) {
		this.state = new byte[200];
		this.dataQueue = new byte[192];
		this.C = new long[5];
		this.tempA = new long[25];
		this.chiC = new long[5];
		System.arraycopy(var1.state, 0, this.state, 0, var1.state.length);
		System.arraycopy(var1.dataQueue, 0, this.dataQueue, 0, var1.dataQueue.length);
		this.rate = var1.rate;
		this.bitsInQueue = var1.bitsInQueue;
		this.fixedOutputLength = var1.fixedOutputLength;
		this.squeezing = var1.squeezing;
		this.bitsAvailableForSqueezing = var1.bitsAvailableForSqueezing;
		this.chunk = Arrays.clone(var1.chunk);
		this.oneByte = Arrays.clone(var1.oneByte);
	}

	private void KeccakAbsorb(byte[] var1, byte[] var2, int var3) {
		this.keccakPermutationAfterXor(var1, var2, var3);
	}

	private void KeccakExtract(byte[] var1, byte[] var2, int var3) {
		System.arraycopy(var1, 0, var2, 0, var3 * 8);
	}

	private void KeccakExtract1024bits(byte[] var1, byte[] var2) {
		System.arraycopy(var1, 0, var2, 0, 128);
	}

	private static boolean LFSR86540(byte[] var0) {
		boolean var1;
		if ((1 & var0[0]) != 0) {
			var1 = true;
		} else {
			var1 = false;
		}

		if ((128 & var0[0]) != 0) {
			var0[0] = (byte) (113 ^ var0[0] << 1);
			return var1;
		} else {
			var0[0] = (byte) (var0[0] << 1);
			return var1;
		}
	}

	private void absorbQueue() {
		this.KeccakAbsorb(this.state, this.dataQueue, this.rate / 8);
		this.bitsInQueue = 0;
	}

	private void chi(long[] var1) {
		for (int var2 = 0; var2 < 5; ++var2) {
			for (int var3 = 0; var3 < 5; ++var3) {
				this.chiC[var3] = var1[var3 + var2 * 5]
						^ ~var1[(var3 + 1) % 5 + var2 * 5] & var1[(var3 + 2) % 5 + var2 * 5];
			}

			for (int var4 = 0; var4 < 5; ++var4) {
				var1[var4 + var2 * 5] = this.chiC[var4];
			}
		}

	}

	private void clearDataQueueSection(int var1, int var2) {
		for (int var3 = var1; var3 != var1 + var2; ++var3) {
			this.dataQueue[var3] = 0;
		}

	}

	private void fromBytesToWords(long[] var1, byte[] var2) {
		for (int var3 = 0; var3 < 25; ++var3) {
			var1[var3] = 0L;
			int var4 = var3 * 8;

			for (int var5 = 0; var5 < 8; ++var5) {
				var1[var3] |= (255L & (long) var2[var4 + var5]) << var5 * 8;
			}
		}

	}

	private void fromWordsToBytes(byte[] var1, long[] var2) {
		for (int var3 = 0; var3 < 25; ++var3) {
			int var4 = var3 * 8;

			for (int var5 = 0; var5 < 8; ++var5) {
				var1[var4 + var5] = (byte) ((int) (255L & var2[var3] >>> var5 * 8));
			}
		}

	}

	private void init(int var1) {
		switch (var1) {
		case 128:
			this.initSponge(1344, 256);
			return;
		case 224:
			this.initSponge(1152, 448);
			return;
		case 256:
			this.initSponge(1088, 512);
			return;
		case 288:
			this.initSponge(1024, 576);
			return;
		case 384:
			this.initSponge(832, 768);
			return;
		case 512:
			this.initSponge(576, 1024);
			return;
		default:
			throw new IllegalArgumentException("bitLength must be one of 128, 224, 256, 288, 384, or 512.");
		}
	}

	private void initSponge(int var1, int var2) {
		if (var1 + var2 != 1600) {
			throw new IllegalStateException("rate + capacity != 1600");
		} else if (var1 > 0 && var1 < 1600 && var1 % 64 == 0) {
			this.rate = var1;
			Arrays.fill((byte[]) this.state, (byte) 0);
			Arrays.fill((byte[]) this.dataQueue, (byte) 0);
			this.bitsInQueue = 0;
			this.squeezing = false;
			this.bitsAvailableForSqueezing = 0;
			this.fixedOutputLength = var2 / 2;
			this.chunk = new byte[var1 / 8];
			this.oneByte = new byte[1];
		} else {
			throw new IllegalStateException("invalid rate value");
		}
	}

	private void iota(long[] var1, int var2) {
		var1[0] ^= KeccakRoundConstants[var2];
	}

	private static int[] keccakInitializeRhoOffsets() {
		int var0 = 0;
		int[] var1 = new int[25];
		var1[0] = 0;
		int var2 = 1;

		int var4;
		for (int var3 = 0; var0 < 24; var2 = var4) {
			var1[var2 % 5 + 5 * (var3 % 5)] = (var0 + 1) * (var0 + 2) / 2 % 64;
			var4 = (var2 * 0 + var3 * 1) % 5;
			var3 = (var2 * 2 + var3 * 3) % 5;
			++var0;
		}

		return var1;
	}

	private static long[] keccakInitializeRoundConstants() {
		long[] var0 = new long[24];
		byte[] var1 = new byte[] { 1 };

		for (int var2 = 0; var2 < 24; ++var2) {
			var0[var2] = 0L;

			for (int var3 = 0; var3 < 7; ++var3) {
				int var4 = -1 + (1 << var3);
				if (LFSR86540(var1)) {
					var0[var2] ^= 1L << var4;
				}
			}
		}

		return var0;
	}

	private void keccakPermutation(byte[] var1) {
		long[] var2 = new long[var1.length / 8];
		this.fromBytesToWords(var2, var1);
		this.keccakPermutationOnWords(var2);
		this.fromWordsToBytes(var1, var2);
	}

	private void keccakPermutationAfterXor(byte[] var1, byte[] var2, int var3) {
		for (int var4 = 0; var4 < var3; ++var4) {
			var1[var4] ^= var2[var4];
		}

		this.keccakPermutation(var1);
	}

	private void keccakPermutationOnWords(long[] var1) {
		for (int var2 = 0; var2 < 24; ++var2) {
			this.theta(var1);
			this.rho(var1);
			this.pi(var1);
			this.chi(var1);
			this.iota(var1, var2);
		}

	}

	private void padAndSwitchToSqueezingPhase() {
		if (1 + this.bitsInQueue == this.rate) {
			byte[] var5 = this.dataQueue;
			int var6 = this.bitsInQueue / 8;
			var5[var6] = (byte) (var5[var6] | 1 << this.bitsInQueue % 8);
			this.absorbQueue();
			this.clearDataQueueSection(0, this.rate / 8);
		} else {
			this.clearDataQueueSection((7 + this.bitsInQueue) / 8, this.rate / 8 - (7 + this.bitsInQueue) / 8);
			byte[] var1 = this.dataQueue;
			int var2 = this.bitsInQueue / 8;
			var1[var2] = (byte) (var1[var2] | 1 << this.bitsInQueue % 8);
		}

		byte[] var3 = this.dataQueue;
		int var4 = (-1 + this.rate) / 8;
		var3[var4] = (byte) (var3[var4] | 1 << (-1 + this.rate) % 8);
		this.absorbQueue();
		if (this.rate == 1024) {
			this.KeccakExtract1024bits(this.state, this.dataQueue);
			this.bitsAvailableForSqueezing = 1024;
		} else {
			this.KeccakExtract(this.state, this.dataQueue, this.rate / 64);
			this.bitsAvailableForSqueezing = this.rate;
		}

		this.squeezing = true;
	}

	private void pi(long[] var1) {
		System.arraycopy(var1, 0, this.tempA, 0, this.tempA.length);

		for (int var2 = 0; var2 < 5; ++var2) {
			for (int var3 = 0; var3 < 5; ++var3) {
				var1[var3 + 5 * ((var2 * 2 + var3 * 3) % 5)] = this.tempA[var2 + var3 * 5];
			}
		}

	}

	private void rho(long[] var1) {
		for (int var2 = 0; var2 < 5; ++var2) {
			for (int var3 = 0; var3 < 5; ++var3) {
				int var4 = var2 + var3 * 5;
				long var5;
				if (KeccakRhoOffsets[var4] != 0) {
					var5 = var1[var4] << KeccakRhoOffsets[var4] ^ var1[var4] >>> 64 - KeccakRhoOffsets[var4];
				} else {
					var5 = var1[var4];
				}

				var1[var4] = var5;
			}
		}

	}

	private void theta(long[] var1) {
		for (int var2 = 0; var2 < 5; ++var2) {
			this.C[var2] = 0L;

			for (int var3 = 0; var3 < 5; ++var3) {
				long[] var4 = this.C;
				var4[var2] ^= var1[var2 + var3 * 5];
			}
		}

		for (int var5 = 0; var5 < 5; ++var5) {
			long var6 = this.C[(var5 + 1) % 5] << 1 ^ this.C[(var5 + 1) % 5] >>> 63 ^ this.C[(var5 + 4) % 5];

			for (int var8 = 0; var8 < 5; ++var8) {
				int var9 = var5 + var8 * 5;
				var1[var9] ^= var6;
			}
		}

	}

	protected void absorb(byte[] var1, int var2, long var3) {
		if (this.bitsInQueue % 8 != 0) {
			throw new IllegalStateException("attempt to absorb with odd length queue.");
		} else if (this.squeezing) {
			throw new IllegalStateException("attempt to absorb while squeezing.");
		} else {
			long var5 = 0L;

			while (true) {
				while (var5 < var3) {
					if (this.bitsInQueue == 0 && var3 >= (long) this.rate && var5 <= var3 - (long) this.rate) {
						long var13 = (var3 - var5) / (long) this.rate;

						for (long var15 = 0L; var15 < var13; ++var15) {
							System.arraycopy(var1, (int) ((long) var2 + var5 / 8L + var15 * (long) this.chunk.length),
									this.chunk, 0, this.chunk.length);
							this.KeccakAbsorb(this.state, this.chunk, this.chunk.length);
						}

						var5 += var13 * (long) this.rate;
					} else {
						int var7 = (int) (var3 - var5);
						if (var7 + this.bitsInQueue > this.rate) {
							var7 = this.rate - this.bitsInQueue;
						}

						int var8 = var7 % 8;
						int var9 = var7 - var8;
						System.arraycopy(var1, var2 + (int) (var5 / 8L), this.dataQueue, this.bitsInQueue / 8,
								var9 / 8);
						this.bitsInQueue += var9;
						long var10 = var5 + (long) var9;
						if (this.bitsInQueue == this.rate) {
							this.absorbQueue();
						}

						if (var8 > 0) {
							int var12 = -1 + (1 << var8);
							this.dataQueue[this.bitsInQueue / 8] = (byte) (var12 & var1[var2 + (int) (var10 / 8L)]);
							this.bitsInQueue += var8;
							var5 = var10 + (long) var8;
						} else {
							var5 = var10;
						}
					}
				}

				return;
			}
		}
	}

	public int doFinal(byte[] var1, int var2) {
		this.squeeze(var1, var2, (long) this.fixedOutputLength);
		this.reset();
		return this.getDigestSize();
	}

	protected int doFinal(byte[] var1, int var2, byte var3, int var4) {
		if (var4 > 0) {
			this.oneByte[0] = var3;
			this.absorb(this.oneByte, 0, (long) var4);
		}

		this.squeeze(var1, var2, (long) this.fixedOutputLength);
		this.reset();
		return this.getDigestSize();
	}

	public String getAlgorithmName() {
		return "Keccak-" + this.fixedOutputLength;
	}

	public int getByteLength() {
		return this.rate / 8;
	}

	public int getDigestSize() {
		return this.fixedOutputLength / 8;
	}

	public void reset() {
		this.init(this.fixedOutputLength);
	}

	protected void squeeze(byte[] var1, int var2, long var3) {
		if (!this.squeezing) {
			this.padAndSwitchToSqueezingPhase();
		}

		if (var3 % 8L != 0L) {
			throw new IllegalStateException("outputLength not a multiple of 8");
		} else {
			int var7;
			for (long var5 = 0L; var5 < var3; var5 += (long) var7) {
				if (this.bitsAvailableForSqueezing == 0) {
					this.keccakPermutation(this.state);
					if (this.rate == 1024) {
						this.KeccakExtract1024bits(this.state, this.dataQueue);
						this.bitsAvailableForSqueezing = 1024;
					} else {
						this.KeccakExtract(this.state, this.dataQueue, this.rate / 64);
						this.bitsAvailableForSqueezing = this.rate;
					}
				}

				var7 = this.bitsAvailableForSqueezing;
				if ((long) var7 > var3 - var5) {
					var7 = (int) (var3 - var5);
				}

				System.arraycopy(this.dataQueue, (this.rate - this.bitsAvailableForSqueezing) / 8, var1,
						var2 + (int) (var5 / 8L), var7 / 8);
				this.bitsAvailableForSqueezing -= var7;
			}

		}
	}

	public void update(byte var1) {
		this.oneByte[0] = var1;
		this.absorb(this.oneByte, 0, 8L);
	}

	public void update(byte[] var1, int var2, int var3) {
		this.absorb(var1, var2, 8L * (long) var3);
	}
}
