package org.spongycastle.crypto.digests;

import org.spongycastle.crypto.ExtendedDigest;
import org.spongycastle.util.Memoable;
import org.spongycastle.util.Pack;

public abstract class GeneralDigest implements ExtendedDigest, Memoable {
	private static final int BYTE_LENGTH = 64;
	private long byteCount;
	private final byte[] xBuf = new byte[4];
	private int xBufOff;

	protected GeneralDigest() {
		this.xBufOff = 0;
	}

	protected GeneralDigest(GeneralDigest var1) {
		this.copyIn(var1);
	}

	protected GeneralDigest(byte[] var1) {
		System.arraycopy(var1, 0, this.xBuf, 0, this.xBuf.length);
		this.xBufOff = Pack.bigEndianToInt(var1, 4);
		this.byteCount = Pack.bigEndianToLong(var1, 8);
	}

	protected void copyIn(GeneralDigest var1) {
		System.arraycopy(var1.xBuf, 0, this.xBuf, 0, var1.xBuf.length);
		this.xBufOff = var1.xBufOff;
		this.byteCount = var1.byteCount;
	}

	public void finish() {
		long var1 = this.byteCount << 3;
		this.update((byte) -128);

		while (this.xBufOff != 0) {
			this.update((byte) 0);
		}

		this.processLength(var1);
		this.processBlock();
	}

	public int getByteLength() {
		return 64;
	}

	protected void populateState(byte[] var1) {
		System.arraycopy(this.xBuf, 0, var1, 0, this.xBufOff);
		Pack.intToBigEndian(this.xBufOff, var1, 4);
		Pack.longToBigEndian(this.byteCount, var1, 8);
	}

	protected abstract void processBlock();

	protected abstract void processLength(long var1);

	protected abstract void processWord(byte[] var1, int var2);

	public void reset() {
		this.byteCount = 0L;
		this.xBufOff = 0;

		for (int var1 = 0; var1 < this.xBuf.length; ++var1) {
			this.xBuf[var1] = 0;
		}

	}

	public void update(byte var1) {
		byte[] var2 = this.xBuf;
		int var3 = this.xBufOff++;
		var2[var3] = var1;
		if (this.xBufOff == this.xBuf.length) {
			this.processWord(this.xBuf, 0);
			this.xBufOff = 0;
		}

		++this.byteCount;
	}

	public void update(byte[] var1, int var2, int var3) {
		int var4 = Math.max(0, var3);
		int var5;
		if (this.xBufOff != 0) {
			label41: {
				int var10;
				for (var10 = 0; var10 < var4; var10 = var5) {
					byte[] var11 = this.xBuf;
					int var12 = this.xBufOff++;
					var5 = var10 + 1;
					var11[var12] = var1[var10 + var2];
					if (this.xBufOff == 4) {
						this.processWord(this.xBuf, 0);
						this.xBufOff = 0;
						break label41;
					}
				}

				var5 = var10;
			}
		} else {
			var5 = 0;
		}

		for (int var6 = var5 + (-4 & var4 - var5); var5 < var6; var5 += 4) {
			this.processWord(var1, var2 + var5);
		}

		while (var5 < var4) {
			byte[] var7 = this.xBuf;
			int var8 = this.xBufOff++;
			int var9 = var5 + 1;
			var7[var8] = var1[var5 + var2];
			var5 = var9;
		}

		this.byteCount += (long) var4;
	}
}
