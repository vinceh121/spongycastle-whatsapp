package org.spongycastle.crypto;

public interface SkippingCipher {
	long getPosition();

	long seekTo(long var1);

	long skip(long var1);
}
