package org.spongycastle.crypto;

public class BufferedBlockCipher {
	protected byte[] buf;
	protected int bufOff;
	protected BlockCipher cipher;
	protected boolean forEncryption;
	protected boolean partialBlockOkay;
	protected boolean pgpCFB;

	protected BufferedBlockCipher() {
	}

	public BufferedBlockCipher(BlockCipher var1) {
		super();
		boolean var2 = true;
		this.cipher = var1;
		this.buf = new byte[var1.getBlockSize()];
		this.bufOff = 0;
		String var3 = var1.getAlgorithmName();
		int var4 = 1 + var3.indexOf(47);
		boolean var5;
		if (var4 > 0 && var3.startsWith("PGP", var4)) {
			var5 = var2;
		} else {
			var5 = false;
		}

		this.pgpCFB = var5;
		if (!this.pgpCFB && !(var1 instanceof StreamCipher)) {
			if (var4 <= 0 || !var3.startsWith("OpenPGP", var4)) {
				var2 = false;
			}

			this.partialBlockOkay = var2;
		} else {
			this.partialBlockOkay = var2;
		}
	}

	public int doFinal(byte[] paramArrayOfByte, int paramInt) throws CryptoException {
		int i = 0;
		try {
			if (paramInt + this.bufOff > paramArrayOfByte.length)
				throw new OutputLengthException("output buffer too short for doFinal()");
		} finally {
			reset();
		}
		int j = 0;
		if (i != 0) {
			if (!this.partialBlockOkay)
				throw new DataLengthException("data not block size aligned");
			this.cipher.processBlock(this.buf, 0, this.buf, 0);
			j = this.bufOff;
			this.bufOff = 0;
			System.arraycopy(this.buf, 0, paramArrayOfByte, paramInt, j);
		}
		reset();
		return j;
	}

	public int getBlockSize() {
		return this.cipher.getBlockSize();
	}

	public int getOutputSize(int var1) {
		return var1 + this.bufOff;
	}

	public BlockCipher getUnderlyingCipher() {
		return this.cipher;
	}

	public int getUpdateOutputSize(int var1) {
		int var2 = var1 + this.bufOff;
		int var3;
		if (this.pgpCFB) {
			if (this.forEncryption) {
				var3 = var2 % this.buf.length - (2 + this.cipher.getBlockSize());
			} else {
				var3 = var2 % this.buf.length;
			}
		} else {
			var3 = var2 % this.buf.length;
		}

		return var2 - var3;
	}

	public void init(boolean var1, CipherParameters var2) {
		this.forEncryption = var1;
		this.reset();
		this.cipher.init(var1, var2);
	}

	public int processByte(byte var1, byte[] var2, int var3) {
		byte[] var4 = this.buf;
		int var5 = this.bufOff++;
		var4[var5] = var1;
		if (this.bufOff == this.buf.length) {
			int var6 = this.cipher.processBlock(this.buf, 0, var2, var3);
			this.bufOff = 0;
			return var6;
		} else {
			return 0;
		}
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		if (var3 < 0) {
			throw new IllegalArgumentException("Can't have a negative input length!");
		} else {
			int var6 = this.getBlockSize();
			int var7 = this.getUpdateOutputSize(var3);
			if (var7 > 0 && var7 + var5 > var4.length) {
				throw new OutputLengthException("output buffer too short");
			} else {
				int var8 = this.buf.length - this.bufOff;
				int var9;
				int var10;
				int var11;
				if (var3 > var8) {
					System.arraycopy(var1, var2, this.buf, this.bufOff, var8);
					var11 = 0 + this.cipher.processBlock(this.buf, 0, var4, var5);
					this.bufOff = 0;
					var9 = var3 - var8;

					for (var10 = var8 + var2; var9 > this.buf.length; var10 += var6) {
						var11 += this.cipher.processBlock(var1, var10, var4, var5 + var11);
						var9 -= var6;
					}
				} else {
					var9 = var3;
					var10 = var2;
					var11 = 0;
				}

				System.arraycopy(var1, var10, this.buf, this.bufOff, var9);
				this.bufOff += var9;
				if (this.bufOff == this.buf.length) {
					var11 += this.cipher.processBlock(this.buf, 0, var4, var5 + var11);
					this.bufOff = 0;
				}

				return var11;
			}
		}
	}

	public void reset() {
		for (int var1 = 0; var1 < this.buf.length; ++var1) {
			this.buf[var1] = 0;
		}

		this.bufOff = 0;
		this.cipher.reset();
	}
}
