package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.jcajce.provider.symmetric.util.BlockCipherProvider;

class AES$ECB$1 implements BlockCipherProvider {
	public BlockCipher get() {
		return new AESFastEngine();
	}
}
