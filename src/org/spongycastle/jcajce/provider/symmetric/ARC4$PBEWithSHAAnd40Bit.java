package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.engines.RC4Engine;
import org.spongycastle.jcajce.provider.symmetric.util.BaseStreamCipher;

public class ARC4$PBEWithSHAAnd40Bit extends BaseStreamCipher {
	public ARC4$PBEWithSHAAnd40Bit() {
		super(new RC4Engine(), 0, 40, 1);
	}
}
