package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class AES$Mappings extends SymmetricAlgorithmProvider {
	private static final String PREFIX = AES.class.getName();
	private static final String wrongAES128 = "2.16.840.1.101.3.4.2";
	private static final String wrongAES192 = "2.16.840.1.101.3.4.22";
	private static final String wrongAES256 = "2.16.840.1.101.3.4.42";

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("AlgorithmParameters.AES", PREFIX + "$AlgParams");
		var1.addAlgorithm("AlgorithmParameters.GCM", PREFIX + "$AlgParamsGCM");
		var1.addAlgorithm("AlgorithmParameterGenerator.AES", PREFIX + "$AlgParamGen");
		var1.addAlgorithm("Cipher.AES", PREFIX + "$ECB");
		var1.addAlgorithm("AlgorithmParameterGenerator.GCM", PREFIX + "$AlgParamGenGCM");
		var1.addAlgorithm("Cipher.GCM", PREFIX + "$GCM");
		var1.addAlgorithm("KeyGenerator.AES", PREFIX + "$KeyGen");
	}
}
