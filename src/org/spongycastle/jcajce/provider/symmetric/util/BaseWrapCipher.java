package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.InvalidCipherTextException;
import org.spongycastle.crypto.Wrapper;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.crypto.params.ParametersWithRandom;
import org.spongycastle.jcajce.util.BCJcaJceHelper;
import org.spongycastle.jcajce.util.JcaJceHelper;
import org.spongycastle.util.Arrays;

public abstract class BaseWrapCipher extends CipherSpi implements PBE {
	private Class[] availableSpecs;
	protected AlgorithmParameters engineParams;
	private final JcaJceHelper helper;
	private byte[] iv;
	private int ivSize;
	protected int pbeHash;
	protected int pbeIvSize;
	protected int pbeKeySize;
	protected int pbeType;
	protected Wrapper wrapEngine;

	protected BaseWrapCipher() {
		this.availableSpecs = new Class[] { IvParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class,
				RC5ParameterSpec.class };
		this.pbeType = 2;
		this.pbeHash = 1;
		this.engineParams = null;
		this.wrapEngine = null;
		this.helper = new BCJcaJceHelper();
	}

	protected BaseWrapCipher(Wrapper var1) {
		this(var1, 0);
	}

	protected BaseWrapCipher(Wrapper var1, int var2) {
		this.availableSpecs = new Class[] { IvParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class,
				RC5ParameterSpec.class };
		this.pbeType = 2;
		this.pbeHash = 1;
		this.engineParams = null;
		this.wrapEngine = null;
		this.helper = new BCJcaJceHelper();
		this.wrapEngine = var1;
		this.ivSize = var2;
	}

	protected final AlgorithmParameters createParametersInstance(String var1) throws NoSuchAlgorithmException, NoSuchProviderException {
		return this.helper.createAlgorithmParameters(var1);
	}

	protected int engineDoFinal(byte[] var1, int var2, int var3, byte[] var4, int var5)
			throws ShortBufferException, IllegalBlockSizeException {
		return 0;
	}

	protected byte[] engineDoFinal(byte[] var1, int var2, int var3) throws IllegalBlockSizeException, BadPaddingException {
		return null;
	}

	protected int engineGetBlockSize() {
		return 0;
	}

	protected byte[] engineGetIV() {
		return Arrays.clone(this.iv);
	}

	protected int engineGetKeySize(Key var1) {
		return var1.getEncoded().length;
	}

	protected int engineGetOutputSize(int var1) {
		return -1;
	}

	protected AlgorithmParameters engineGetParameters() {
		return null;
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameters var3, SecureRandom var4)
			throws InvalidAlgorithmParameterException, InvalidKeyException {
		AlgorithmParameterSpec var5;
		if (var3 != null) {
			int var6 = 0;

			while (true) {
				if (var6 == this.availableSpecs.length) {
					var5 = null;
					break;
				}

				AlgorithmParameterSpec var8;
				try {
					var8 = var3.getParameterSpec(this.availableSpecs[var6]);
				} catch (Exception var9) {
					++var6;
					continue;
				}

				var5 = var8;
				break;
			}

			if (var5 == null) {
				throw new InvalidAlgorithmParameterException("can't handle parameter " + var3.toString());
			}
		} else {
			var5 = null;
		}

		this.engineParams = var3;
		this.engineInit(var1, var2, var5, var4);
	}

	protected void engineInit(int var1, Key var2, SecureRandom var3) throws InvalidKeyException {
		try {
			this.engineInit(var1, var2, (AlgorithmParameterSpec) null, var3);
		} catch (InvalidAlgorithmParameterException var5) {
			throw new IllegalArgumentException(var5.getMessage());
		}
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameterSpec var3, SecureRandom var4)
			throws InvalidAlgorithmParameterException, InvalidKeyException {
		Object var5;
		if (var2 instanceof BCPBEKey) {
			BCPBEKey var8 = (BCPBEKey) var2;
			if (var3 instanceof PBEParameterSpec) {
				var5 = PBE$Util.makePBEParameters(var8, var3, this.wrapEngine.getAlgorithmName());
			} else {
				if (var8.getParam() == null) {
					throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
				}

				var5 = var8.getParam();
			}
		} else {
			var5 = new KeyParameter(var2.getEncoded());
		}

		if (var3 instanceof IvParameterSpec) {
			var5 = new ParametersWithIV((CipherParameters) var5, ((IvParameterSpec) var3).getIV());
		}

		Object var6;
		if (var5 instanceof KeyParameter && this.ivSize != 0) {
			this.iv = new byte[this.ivSize];
			var4.nextBytes(this.iv);
			var6 = new ParametersWithIV((CipherParameters) var5, this.iv);
		} else {
			var6 = var5;
		}

		Object var7;
		if (var4 != null) {
			var7 = new ParametersWithRandom((CipherParameters) var6, var4);
		} else {
			var7 = var6;
		}

		switch (var1) {
		case 1:
		case 2:
			throw new IllegalArgumentException("engine only valid for wrapping");
		case 3:
			this.wrapEngine.init(true, (CipherParameters) var7);
			return;
		case 4:
			this.wrapEngine.init(false, (CipherParameters) var7);
			return;
		default:
			System.out.println("eeek!");
		}
	}

	protected void engineSetMode(String var1) throws NoSuchAlgorithmException {
		throw new NoSuchAlgorithmException("can't support mode " + var1);
	}

	protected void engineSetPadding(String var1) throws NoSuchPaddingException {
		throw new NoSuchPaddingException("Padding " + var1 + " unknown.");
	}

	protected Key engineUnwrap(byte[] var1, String var2, int var3) throws InvalidKeyException, NoSuchAlgorithmException {
		byte[] var8;
		boolean var10001;
		label82: {
			InvalidCipherTextException var27;
			label81: {
				BadPaddingException var26;
				label80: {
					byte[] var13;
					label79: {
						IllegalBlockSizeException var10000;
						label85: {
							try {
								if (this.wrapEngine == null) {
									var13 = this.engineDoFinal(var1, 0, var1.length);
									break label79;
								}
//							} catch (InvalidCipherTextException var23) {
//								var27 = var23;
//								var10001 = false;
//								break label81;
							} catch (BadPaddingException var24) {
								var26 = var24;
								var10001 = false;
								break label80;
							} catch (IllegalBlockSizeException var25) {
								var10000 = var25;
								var10001 = false;
								break label85;
							}

							byte[] var7;
							try {
								var7 = this.wrapEngine.unwrap(var1, 0, var1.length);
							} catch (InvalidCipherTextException var20) {
								var27 = var20;
								var10001 = false;
								break label81;
							} catch (BadPaddingException var21) {
								var26 = var21;
								var10001 = false;
								break label80;
							} catch (IllegalBlockSizeException var22) {
								var10000 = var22;
								var10001 = false;
								break label85;
							}

							var8 = var7;
							break label82;
						}

						IllegalBlockSizeException var4 = var10000;
						throw new InvalidKeyException(var4.getMessage());
					}

					var8 = var13;
					break label82;
				}

				BadPaddingException var5 = var26;
				throw new InvalidKeyException(var5.getMessage());
			}

			InvalidCipherTextException var6 = var27;
			throw new InvalidKeyException(var6.getMessage());
		}

		if (var3 == 3) {
			return new SecretKeySpec(var8, var2);
		} else {
			InvalidKeySpecException var28;
			label62: {
				NoSuchProviderException var29;
				label61: {
					KeyFactory var11;
					try {
						var11 = this.helper.createKeyFactory(var2);
					} catch (NoSuchProviderException var18) {
						var29 = var18;
						var10001 = false;
						break label61;
					} catch (InvalidKeySpecException var19) {
						var28 = var19;
						var10001 = false;
						break label62;
					}

					if (var3 == 1) {
						try {
							return var11.generatePublic(new X509EncodedKeySpec(var8));
//						} catch (NoSuchProviderException var14) {
//							var29 = var14;
//							var10001 = false;
						} catch (InvalidKeySpecException var15) {
							var28 = var15;
							var10001 = false;
							break label62;
						}
					} else {
						if (var3 != 2) {
							throw new InvalidKeyException("Unknown key type " + var3);
						}

						try {
							PrivateKey var12 = var11.generatePrivate(new PKCS8EncodedKeySpec(var8));
							return var12;
//						} catch (NoSuchProviderException var16) {
//							var29 = var16;
//							var10001 = false;
						} catch (InvalidKeySpecException var17) {
							var28 = var17;
							var10001 = false;
							break label62;
						}
					}
				}

				NoSuchProviderException var10 = var29;
				throw new InvalidKeyException("Unknown key type " + var10.getMessage());
			}

			InvalidKeySpecException var9 = var28;
			throw new InvalidKeyException("Unknown key type " + var9.getMessage());
		}
	}

	protected int engineUpdate(byte[] var1, int var2, int var3, byte[] var4, int var5) throws ShortBufferException {
		throw new RuntimeException("not supported for wrapping");
	}

	protected byte[] engineUpdate(byte[] var1, int var2, int var3) {
		throw new RuntimeException("not supported for wrapping");
	}

	protected byte[] engineWrap(Key var1) throws InvalidKeyException, IllegalBlockSizeException {
		byte[] var2 = var1.getEncoded();
		if (var2 == null) {
			throw new InvalidKeyException("Cannot wrap key, null encoding.");
		} else {
			try {
				if (this.wrapEngine == null) {
					return this.engineDoFinal(var2, 0, var2.length);
				} else {
					byte[] var4 = this.wrapEngine.wrap(var2, 0, var2.length);
					return var4;
				}
			} catch (BadPaddingException var5) {
				throw new IllegalBlockSizeException(var5.getMessage());
			}
		}
	}
}
