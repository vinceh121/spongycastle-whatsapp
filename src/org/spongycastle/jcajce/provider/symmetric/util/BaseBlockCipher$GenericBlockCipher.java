package org.spongycastle.jcajce.provider.symmetric.util;

import javax.crypto.BadPaddingException;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;

interface BaseBlockCipher$GenericBlockCipher {
	int doFinal(byte[] var1, int var2) throws CryptoException, BadPaddingException;

	String getAlgorithmName();

	int getOutputSize(int var1);

	BlockCipher getUnderlyingCipher();

	int getUpdateOutputSize(int var1);

	void init(boolean var1, CipherParameters var2);

	int processByte(byte var1, byte[] var2, int var3);

	int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5);

	void updateAAD(byte[] var1, int var2, int var3);

	boolean wrapOnNoPadding();
}
