package org.spongycastle.jcajce.provider.symmetric.util;

import java.lang.reflect.Constructor;
import javax.crypto.BadPaddingException;
import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.InvalidCipherTextException;
import org.spongycastle.crypto.modes.AEADBlockCipher;

class BaseBlockCipher$AEADGenericBlockCipher implements BaseBlockCipher$GenericBlockCipher {
	private static final Constructor aeadBadTagConstructor;
	private AEADBlockCipher cipher;

	static {
		Class var0 = BaseBlockCipher.access$0("javax.crypto.AEADBadTagException");
		if (var0 != null) {
			aeadBadTagConstructor = findExceptionConstructor(var0);
		} else {
			aeadBadTagConstructor = null;
		}
	}

	BaseBlockCipher$AEADGenericBlockCipher(AEADBlockCipher var1) {
		this.cipher = var1;
	}

	private static Constructor findExceptionConstructor(Class var0) {
		try {
			Constructor var2 = var0.getConstructor(String.class);
			return var2;
		} catch (Exception var3) {
			return null;
		}
	}

	public int doFinal(byte[] var1, int var2) throws CryptoException, BadPaddingException {
		try {
			int var8 = this.cipher.doFinal(var1, var2);
			return var8;
		} catch (InvalidCipherTextException var10) {
			InvalidCipherTextException var3 = var10;
			if (aeadBadTagConstructor != null) {
				BadPaddingException var5;
				try {
					Constructor var6 = aeadBadTagConstructor;
					Object[] var7 = new Object[] { var3.getMessage() };
					var5 = (BadPaddingException) var6.newInstance(var7);
				} catch (Exception var9) {
					var5 = null;
				}

				if (var5 != null) {
					throw var5;
				}
			}

			throw new BadPaddingException(var10.getMessage());
		}
	}

	public String getAlgorithmName() {
		return this.cipher.getUnderlyingCipher().getAlgorithmName();
	}

	public int getOutputSize(int var1) {
		return this.cipher.getOutputSize(var1);
	}

	public BlockCipher getUnderlyingCipher() {
		return this.cipher.getUnderlyingCipher();
	}

	public int getUpdateOutputSize(int var1) {
		return this.cipher.getUpdateOutputSize(var1);
	}

	public void init(boolean var1, CipherParameters var2) {
		this.cipher.init(var1, var2);
	}

	public int processByte(byte var1, byte[] var2, int var3) {
		return this.cipher.processByte(var1, var2, var3);
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		return this.cipher.processBytes(var1, var2, var3, var4, var5);
	}

	public void updateAAD(byte[] var1, int var2, int var3) {
		this.cipher.processAADBytes(var1, var2, var3);
	}

	public boolean wrapOnNoPadding() {
		return false;
	}
}
