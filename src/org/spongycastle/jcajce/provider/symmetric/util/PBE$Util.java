package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.PBEParametersGenerator;

public class PBE$Util {
	private static byte[] convertPassword(int var0, PBEKeySpec var1) {
		if (var0 == 2) {
			return PBEParametersGenerator.PKCS12PasswordToBytes(var1.getPassword());
		} else {
			return var0 != 5 && var0 != 4 ? PBEParametersGenerator.PKCS5PasswordToBytes(var1.getPassword())
					: PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(var1.getPassword());
		}
	}

	private static PBEParametersGenerator makePBEGenerator(int var0, int var1) {
		return null;
	}

	public static CipherParameters makePBEMacParameters(SecretKey var0, int var1, int var2, int var3,
			PBEParameterSpec var4) {
		PBEParametersGenerator var5 = makePBEGenerator(var1, var2);
		byte[] var6 = var0.getEncoded();
		var5.init(var0.getEncoded(), var4.getSalt(), var4.getIterationCount());
		CipherParameters var7 = var5.generateDerivedMacParameters(var3);

		for (int var8 = 0; var8 != var6.length; ++var8) {
			var6[var8] = 0;
		}

		return var7;
	}

	public static CipherParameters makePBEMacParameters(PBEKeySpec var0, int var1, int var2, int var3) {
		PBEParametersGenerator var4 = makePBEGenerator(var1, var2);
		byte[] var5 = convertPassword(var1, var0);
		var4.init(var5, var0.getSalt(), var0.getIterationCount());
		CipherParameters var6 = var4.generateDerivedMacParameters(var3);

		for (int var7 = 0; var7 != var5.length; ++var7) {
			var5[var7] = 0;
		}

		return var6;
	}

	public static CipherParameters makePBEMacParameters(BCPBEKey var0, AlgorithmParameterSpec var1) {
		if (var1 != null && var1 instanceof PBEParameterSpec) {
			PBEParameterSpec var2 = (PBEParameterSpec) var1;
			byte[] var3 = var0.getEncoded();
			PBEParametersGenerator gen = makePBEGenerator(0, 0); // Compiler optimized this by null cause it
																	// returns null
//			null.init(var3, var2.getSalt(), var2.getIterationCount());
			gen.init(var3, var2.getSalt(), var2.getIterationCount());
//			CipherParameters var4 = null.generateDerivedMacParameters(var0.getKeySize());
			CipherParameters var4 = gen.generateDerivedMacParameters(var0.getKeySize());

			for (int var5 = 0; var5 != var3.length; ++var5) {
				var3[var5] = 0;
			}

			return var4;
		} else {
			throw new IllegalArgumentException("Need a PBEParameter spec with a PBE key.");
		}
	}

	public static CipherParameters makePBEParameters(PBEKeySpec var0, int var1, int var2, int var3, int var4) {
		PBEParametersGenerator var5 = makePBEGenerator(var1, var2);
		byte[] var6 = convertPassword(var1, var0);
		var5.init(var6, var0.getSalt(), var0.getIterationCount());
		CipherParameters var7;
		if (var4 != 0) {
			var7 = var5.generateDerivedParameters(var3, var4);
		} else {
			var7 = var5.generateDerivedParameters(var3);
		}

		for (int var8 = 0; var8 != var6.length; ++var8) {
			var6[var8] = 0;
		}

		return var7;
	}

	public static CipherParameters makePBEParameters(BCPBEKey var0, AlgorithmParameterSpec var1, String var2) {
		if (var1 != null && var1 instanceof PBEParameterSpec) {
			PBEParameterSpec var10000 = (PBEParameterSpec) var1;
			byte[] var4 = var0.getEncoded();
			if (var0.shouldTryWrongPKCS12()) {
				var4 = new byte[2];
			}

			for (int var5 = 0; var5 != var4.length; ++var5) {
				var4[var5] = 0;
			}

			return null;
		} else {
			throw new IllegalArgumentException("Need a PBEParameter spec with a PBE key.");
		}
	}

	public static CipherParameters makePBEParameters(byte[] var0, int var1, int var2, int var3, int var4,
			PBEParameterSpec var5, String var6) {
		return null;
	}
}
