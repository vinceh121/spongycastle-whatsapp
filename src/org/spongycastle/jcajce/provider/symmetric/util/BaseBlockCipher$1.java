package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.InvalidKeyException;

class BaseBlockCipher$1 extends InvalidKeyException {
	// $FF: synthetic field
	final BaseBlockCipher this$0;
	// $FF: synthetic field
	private final Exception val$e;

	BaseBlockCipher$1(BaseBlockCipher var1, String var2, Exception var3) {
		super(var2);
		this.this$0 = var1;
		this.val$e = var3;
	}

	public Throwable getCause() {
		return this.val$e;
	}
}
