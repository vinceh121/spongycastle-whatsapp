package org.spongycastle.jcajce.provider.symmetric.util;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.BufferedBlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.OutputLengthException;
import org.spongycastle.crypto.modes.AEADBlockCipher;
import org.spongycastle.crypto.modes.CFBBlockCipher;
import org.spongycastle.crypto.modes.GCMBlockCipher;
import org.spongycastle.crypto.modes.OFBBlockCipher;
import org.spongycastle.crypto.params.AEADParameters;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.crypto.params.ParametersWithRandom;
import org.spongycastle.util.Strings;

public class BaseBlockCipher extends BaseWrapCipher implements PBE {
	private static final Class gcmSpecClass = lookup("javax.crypto.spec.GCMParameterSpec");
	private AEADParameters aeadParams;
	private Class[] availableSpecs;
	private BlockCipher baseEngine;
	private BaseBlockCipher$GenericBlockCipher cipher;
	private int digest;
	private BlockCipherProvider engineProvider;
	private boolean fixedIv;
	private int ivLength;
	private ParametersWithIV ivParam;
	private int keySizeInBits;
	private String modeName;
	private boolean padded;
	private String pbeAlgorithm;
	private PBEParameterSpec pbeSpec;
	private int scheme;

	protected BaseBlockCipher(BlockCipher var1) {
		Class[] var2 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var2;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1;
		this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(var1);
	}

	protected BaseBlockCipher(BlockCipher var1, int var2) {
		Class[] var3 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var3;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1;
		this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(var1);
		this.ivLength = var2 / 8;
	}

	protected BaseBlockCipher(BlockCipher var1, int var2, int var3, int var4, int var5) {
		Class[] var6 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var6;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1;
		this.scheme = var2;
		this.digest = var3;
		this.keySizeInBits = var4;
		this.ivLength = var5;
		this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(var1);
	}

	protected BaseBlockCipher(BufferedBlockCipher var1, int var2) {
		Class[] var3 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var3;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1.getUnderlyingCipher();
		this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(var1);
		this.ivLength = var2 / 8;
	}

	protected BaseBlockCipher(AEADBlockCipher var1) {
		Class[] var2 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var2;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1.getUnderlyingCipher();
		this.ivLength = this.baseEngine.getBlockSize();
		this.cipher = new BaseBlockCipher$AEADGenericBlockCipher(var1);
	}

	protected BaseBlockCipher(AEADBlockCipher var1, boolean var2, int var3) {
		Class[] var4 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var4;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1.getUnderlyingCipher();
		this.fixedIv = var2;
		this.ivLength = var3;
		this.cipher = new BaseBlockCipher$AEADGenericBlockCipher(var1);
	}

	protected BaseBlockCipher(BlockCipherProvider var1) {
		Class[] var2 = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, gcmSpecClass,
				IvParameterSpec.class, PBEParameterSpec.class };
		this.availableSpecs = var2;
		this.scheme = -1;
		this.ivLength = 0;
		this.fixedIv = true;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.modeName = null;
		this.baseEngine = var1.get();
		this.engineProvider = var1;
		this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(var1.get());
	}

	// $FF: synthetic method
	static Class access$0(String var0) {
		return lookup(var0);
	}

	private CipherParameters adjustParameters(AlgorithmParameterSpec var1, CipherParameters var2) {
		if (var2 instanceof ParametersWithIV) {
			CipherParameters var3 = ((ParametersWithIV) var2).getParameters();
			if (var1 instanceof IvParameterSpec) {
				this.ivParam = new ParametersWithIV(var3, ((IvParameterSpec) var1).getIV());
				var2 = this.ivParam;
			}
		} else if (var1 instanceof IvParameterSpec) {
			this.ivParam = new ParametersWithIV((CipherParameters) var2, ((IvParameterSpec) var1).getIV());
			return this.ivParam;
		}

		return (CipherParameters) var2;
	}

	private boolean isAEADModeName(String var1) {
		return true;
	}

	private static Class lookup(String var0) {
		try {
			Class var2 = BaseBlockCipher.class.getClassLoader().loadClass(var0);
			return var2;
		} catch (Exception var3) {
			return null;
		}
	}

	protected int engineDoFinal(byte[] var1, int var2, int var3, byte[] var4, int var5)
			throws ShortBufferException, IllegalBlockSizeException {
		if (var5 + this.engineGetOutputSize(var3) > var4.length) {
			throw new ShortBufferException("output buffer too short for input.");
		} else {
			OutputLengthException var14;
			label30: {
				DataLengthException var10000;
				label29: {
					int var6 = 0;
					boolean var10001;
					if (var3 != 0) {
						try {
							var6 = this.cipher.processBytes(var1, var2, var3, var4, var5);
						} catch (OutputLengthException var12) {
							var14 = var12;
							var10001 = false;
							break label30;
						} catch (DataLengthException var13) {
							var10000 = var13;
							var10001 = false;
							break label29;
						}
					}

					try {
						int var9;
						try {
							var9 = this.cipher.doFinal(var4, var5 + var6);
							return var6 + var9;
						} catch (BadPaddingException | CryptoException e) {
							e.printStackTrace();
							throw new RuntimeException("Crappy exception handleing brought by decompilation", e);
						}
					} catch (OutputLengthException var10) {
						var14 = var10;
						var10001 = false;
						break label30;
					} catch (DataLengthException var11) {
						var10000 = var11;
						var10001 = false;
					}
				}

				DataLengthException var7 = var10000;
				throw new IllegalBlockSizeException(var7.getMessage());
			}

			OutputLengthException var8 = var14;
			throw new IllegalBlockSizeException(var8.getMessage());
		}
	}

	protected byte[] engineDoFinal(byte[] var1, int var2, int var3) throws IllegalBlockSizeException {
		byte[] var4 = new byte[this.engineGetOutputSize(var3)];
		int var5;
		if (var3 != 0) {
			var5 = this.cipher.processBytes(var1, var2, var3, var4, 0);
		} else {
			var5 = 0;
		}

		int var7;
		try {
			var7 = this.cipher.doFinal(var4, var5);
		} catch (DataLengthException | BadPaddingException | CryptoException var10) {
			throw new IllegalBlockSizeException(var10.getMessage());
		}

		int var8 = var7 + var5;
		if (var8 == var4.length) {
			return var4;
		} else {
			byte[] var9 = new byte[var8];
			System.arraycopy(var4, 0, var9, 0, var8);
			return var9;
		}
	}

	protected int engineGetBlockSize() {
		return this.baseEngine.getBlockSize();
	}

	protected byte[] engineGetIV() {
		if (this.aeadParams != null) {
			return this.aeadParams.getNonce();
		} else {
			return this.ivParam != null ? this.ivParam.getIV() : null;
		}
	}

	protected int engineGetKeySize(Key var1) {
		return 8 * var1.getEncoded().length;
	}

	protected int engineGetOutputSize(int var1) {
		return this.cipher.getOutputSize(var1);
	}

	protected AlgorithmParameters engineGetParameters() {
		if (this.engineParams == null) {
			if (this.pbeSpec != null) {
				try {
					this.engineParams = this.createParametersInstance(this.pbeAlgorithm);
					this.engineParams.init(this.pbeSpec);
				} catch (Exception var5) {
					return null;
				}
			} else if (this.ivParam != null) {
				String var1 = this.cipher.getUnderlyingCipher().getAlgorithmName();
				if (var1.indexOf(47) >= 0) {
					var1 = var1.substring(0, var1.indexOf(47));
				}

				try {
					this.engineParams = this.createParametersInstance(var1);
					this.engineParams.init(this.ivParam.getIV());
				} catch (Exception var4) {
					throw new RuntimeException(var4.toString());
				}
			}
		}

		return this.engineParams;
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameters var3, SecureRandom var4)
			throws InvalidAlgorithmParameterException, InvalidKeyException {
		AlgorithmParameterSpec var5;
		if (var3 != null) {
			int var6 = 0;

			label30: {
				AlgorithmParameterSpec var8;
				while (true) {
					if (var6 == this.availableSpecs.length) {
						var5 = null;
						break label30;
					}

					if (this.availableSpecs[var6] != null) {
						try {
							var8 = var3.getParameterSpec(this.availableSpecs[var6]);
							break;
						} catch (Exception var9) {
						}
					}

					++var6;
				}

				var5 = var8;
			}

			if (var5 == null) {
				throw new InvalidAlgorithmParameterException("can't handle parameter " + var3.toString());
			}
		} else {
			var5 = null;
		}

		this.engineInit(var1, var2, var5, var4);
		this.engineParams = var3;
	}

	protected void engineInit(int var1, Key var2, SecureRandom var3) throws InvalidKeyException {
		try {
			this.engineInit(var1, var2, (AlgorithmParameterSpec) null, var3);
		} catch (InvalidAlgorithmParameterException var5) {
			throw new InvalidKeyException(var5.getMessage());
		}
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameterSpec var3, SecureRandom var4)
			throws InvalidAlgorithmParameterException, InvalidKeyException {
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.engineParams = null;
		this.aeadParams = null;
		if (!(var2 instanceof SecretKey)) {
			throw new InvalidKeyException(
					"Key for algorithm " + var2.getAlgorithm() + " not suitable for symmetric enryption.");
		} else if (var3 == null && this.baseEngine.getAlgorithmName().startsWith("RC5-64")) {
			throw new InvalidAlgorithmParameterException("RC5 requires an RC5ParametersSpec to be passed in.");
		} else {
			Object var5;
			if (var2 instanceof BCPBEKey) {
				BCPBEKey var19 = (BCPBEKey) var2;
				this.pbeAlgorithm = var19.getAlgorithm();
				CipherParameters var20;
				if (var19.getParam() != null) {
					var20 = this.adjustParameters(var3, var19.getParam());
				} else {
					if (!(var3 instanceof PBEParameterSpec)) {
						throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
					}

					this.pbeSpec = (PBEParameterSpec) var3;
					var20 = PBE$Util.makePBEParameters(var19, var3,
							this.cipher.getUnderlyingCipher().getAlgorithmName());
				}

				if (var20 instanceof ParametersWithIV) {
					this.ivParam = (ParametersWithIV) var20;
					var5 = var20;
				} else {
					var5 = var20;
				}
			} else if (!(var2 instanceof RepeatedSecretKeySpec)) {
				if (this.scheme == 0 || this.scheme == 4 || this.scheme == 1 || this.scheme == 5) {
					throw new InvalidKeyException("Algorithm requires a PBE key");
				}

				var5 = new KeyParameter(var2.getEncoded());
			} else {
				var5 = null;
			}

			boolean var10001;
			if (var3 instanceof IvParameterSpec) {
				if (this.ivLength != 0) {
					IvParameterSpec var17 = (IvParameterSpec) var3;
					if (var17.getIV().length != this.ivLength
							&& !(this.cipher instanceof BaseBlockCipher$AEADGenericBlockCipher) && this.fixedIv) {
						throw new InvalidAlgorithmParameterException("IV must be " + this.ivLength + " bytes long.");
					}

					ParametersWithIV var18;
					if (var5 instanceof ParametersWithIV) {
						var18 = new ParametersWithIV(((ParametersWithIV) var5).getParameters(), var17.getIV());
					} else {
						var18 = new ParametersWithIV((CipherParameters) var5, var17.getIV());
					}

					this.ivParam = (ParametersWithIV) var18;
					var5 = var18;
				} else if (this.modeName != null && this.modeName.equals("ECB")) {
					throw new InvalidAlgorithmParameterException("ECB mode does not use an IV");
				}
			} else if (gcmSpecClass != null && gcmSpecClass.isInstance(var3)) {
				if (!this.isAEADModeName(this.modeName)
						&& !(this.cipher instanceof BaseBlockCipher$AEADGenericBlockCipher)) {
					throw new InvalidAlgorithmParameterException("GCMParameterSpec can only be used with AEAD modes.");
				}

				Method var12;
				Method var13;
				KeyParameter var15;
				label182: {
					try {
						var12 = gcmSpecClass.getDeclaredMethod("getTLen");
						var13 = gcmSpecClass.getDeclaredMethod("getIV");
						if (var5 instanceof ParametersWithIV) {
							var15 = (KeyParameter) ((ParametersWithIV) var5).getParameters();
							break label182;
						}
					} catch (Exception var26) {
						var10001 = false;
						throw new InvalidAlgorithmParameterException("Cannot process GCMParameterSpec.");
					}

					KeyParameter var14;
					try {
						var14 = (KeyParameter) var5;
					} catch (Exception var25) {
						var10001 = false;
						throw new InvalidAlgorithmParameterException("Cannot process GCMParameterSpec.");
					}

					var15 = var14;
				}

				AEADParameters var16;
				try {
					var16 = new AEADParameters(var15, (Integer) var12.invoke(var3), (byte[]) var13.invoke(var3));
					this.aeadParams = var16;
				} catch (Exception var24) {
					var10001 = false;
					throw new InvalidAlgorithmParameterException("Cannot process GCMParameterSpec.");
				}

				var5 = var16;
			} else if (var3 != null && !(var3 instanceof PBEParameterSpec)) {
				throw new InvalidAlgorithmParameterException("unknown parameter type.");
			}

			Object var6;
			label136: {
				if (this.ivLength != 0 && !(var5 instanceof ParametersWithIV) && !(var5 instanceof AEADParameters)) {
					SecureRandom var9;
					if (var4 == null) {
						var9 = new SecureRandom();
					} else {
						var9 = var4;
					}

					if (var1 == 1 || var1 == 3) {
						byte[] var10 = new byte[this.ivLength];
						var9.nextBytes(var10);
						var6 = new ParametersWithIV((CipherParameters) var5, var10);
						this.ivParam = (ParametersWithIV) var6;
						break label136;
					}

					if (this.cipher.getUnderlyingCipher().getAlgorithmName().indexOf("PGPCFB") < 0) {
						throw new InvalidAlgorithmParameterException("no IV set when one expected");
					}
				}

				var6 = var5;
			}

			Object var7;
			if (var4 != null && this.padded) {
				var7 = new ParametersWithRandom((CipherParameters) var6, var4);
			} else {
				var7 = var6;
			}

			Exception var10000;
			switch (var1) {
			case 1:
			case 3:
				try {
					this.cipher.init(true, (CipherParameters) var7);
					return;
				} catch (Exception var22) {
					var10000 = var22;
					var10001 = false;
					break;
				}
			case 2:
			case 4:
				try {
					this.cipher.init(false, (CipherParameters) var7);
					return;
				} catch (Exception var21) {
					var10000 = var21;
					var10001 = false;
					break;
				}
			default:
				try {
					throw new InvalidParameterException("unknown opmode " + var1 + " passed");
				} catch (Exception var23) {
					var10000 = var23;
					var10001 = false;
				}
			}

			Exception var8 = var10000;
			throw new BaseBlockCipher$1(this, var8.getMessage(), var8);
		}
	}

	protected void engineSetMode(String var1) throws NoSuchAlgorithmException {
		this.modeName = Strings.toUpperCase(var1);
		if (this.modeName.equals("ECB")) {
			this.ivLength = 0;
			this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(this.baseEngine);
		} else if (this.modeName.equals("CBC")) {
			this.ivLength = this.baseEngine.getBlockSize();
			this.cipher = new BaseBlockCipher$AEADGenericBlockCipher(new GCMBlockCipher(this.baseEngine));
		} else if (this.modeName.startsWith("OFB")) {
			this.ivLength = this.baseEngine.getBlockSize();
			if (this.modeName.length() != 3) {
				int var3 = Integer.parseInt(this.modeName.substring(3));
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(new OFBBlockCipher(this.baseEngine, var3));
			} else {
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(
						new OFBBlockCipher(this.baseEngine, 8 * this.baseEngine.getBlockSize()));
			}
		} else if (this.modeName.startsWith("CFB")) {
			this.ivLength = this.baseEngine.getBlockSize();
			if (this.modeName.length() != 3) {
				int var2 = Integer.parseInt(this.modeName.substring(3));
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(new CFBBlockCipher(this.baseEngine, var2));
			} else {
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(
						new CFBBlockCipher(this.baseEngine, 8 * this.baseEngine.getBlockSize()));
			}
		} else if (this.modeName.startsWith("GCM")) {
			this.ivLength = this.baseEngine.getBlockSize();
			this.cipher = new BaseBlockCipher$AEADGenericBlockCipher(new GCMBlockCipher(this.baseEngine));
		} else {
			throw new NoSuchAlgorithmException("can't support mode " + var1);
		}
	}

	protected void engineSetPadding(String var1) throws NoSuchPaddingException {
		String var2 = Strings.toUpperCase(var1);
		if (var2.equals("NOPADDING")) {
			if (this.cipher.wrapOnNoPadding()) {
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(
						new BufferedBlockCipher(this.cipher.getUnderlyingCipher()));
			}

		} else {
			this.padded = true;
			if (this.isAEADModeName(this.modeName)) {
				throw new NoSuchPaddingException("Only NoPadding can be used with AEAD modes.");
			} else if (!var2.equals("PKCS5PADDING") && !var2.equals("PKCS7PADDING")) {
				throw new NoSuchPaddingException("Padding " + var1 + " unknown.");
			} else {
				this.cipher = new BaseBlockCipher$BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher());
			}
		}
	}

	protected int engineUpdate(byte[] var1, int var2, int var3, byte[] var4, int var5) throws ShortBufferException {
		if (var5 + this.cipher.getUpdateOutputSize(var3) > var4.length) {
			throw new ShortBufferException("output buffer too short for input.");
		} else {
			try {
				int var7 = this.cipher.processBytes(var1, var2, var3, var4, var5);
				return var7;
			} catch (DataLengthException var8) {
				throw new IllegalStateException(var8.toString());
			}
		}
	}

	protected byte[] engineUpdate(byte[] var1, int var2, int var3) {
		int var4 = this.cipher.getUpdateOutputSize(var3);
		if (var4 > 0) {
			byte[] var6 = new byte[var4];
			int var7 = this.cipher.processBytes(var1, var2, var3, var6, 0);
			if (var7 == 0) {
				var6 = null;
			} else if (var7 != var6.length) {
				byte[] var8 = new byte[var7];
				System.arraycopy(var6, 0, var8, 0, var7);
				return var8;
			}

			return var6;
		} else {
			this.cipher.processBytes(var1, var2, var3, (byte[]) null, 0);
			return null;
		}
	}

	protected void engineUpdateAAD(ByteBuffer var1) {
		int var2 = var1.arrayOffset() + var1.position();
		int var3 = var1.limit() - var1.position();
		this.engineUpdateAAD(var1.array(), var2, var3);
	}

	protected void engineUpdateAAD(byte[] var1, int var2, int var3) {
		this.cipher.updateAAD(var1, var2, var3);
	}
}
