package org.spongycastle.jcajce.provider.symmetric.util;

public interface PBE {
	int GOST3411 = 6;
	int MD2 = 5;
	int MD5 = 0;
	int OPENSSL = 3;
	int PKCS12 = 2;
	int PKCS5S1 = 0;
	int PKCS5S1_UTF8 = 4;
	int PKCS5S2 = 1;
	int PKCS5S2_UTF8 = 5;
	int RIPEMD160 = 2;
	int SHA1 = 1;
	int SHA256 = 4;
	int TIGER = 3;
}
