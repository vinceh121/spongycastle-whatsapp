package org.spongycastle.jcajce.provider.symmetric.util;

import javax.crypto.interfaces.PBEKey;
import javax.crypto.spec.PBEKeySpec;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.PBEParametersGenerator;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;

public class BCPBEKey implements PBEKey {
	String algorithm;
	int digest;
	int ivSize;
	int keySize;
	CipherParameters param;
	PBEKeySpec pbeKeySpec;
	boolean tryWrong = false;
	int type;

	public BCPBEKey(String var1, int var2, int var3, int var4, int var5, PBEKeySpec var6, CipherParameters var7) {
		this.algorithm = var1;
		this.type = var2;
		this.digest = var3;
		this.keySize = var4;
		this.ivSize = var5;
		this.pbeKeySpec = var6;
		this.param = var7;
	}

	public String getAlgorithm() {
		return this.algorithm;
	}

	int getDigest() {
		return this.digest;
	}

	public byte[] getEncoded() {
		if (this.param != null) {
			KeyParameter var1;
			if (this.param instanceof ParametersWithIV) {
				var1 = (KeyParameter) ((ParametersWithIV) this.param).getParameters();
			} else {
				var1 = (KeyParameter) this.param;
			}

			return var1.getKey();
		} else if (this.type == 2) {
			return PBEParametersGenerator.PKCS12PasswordToBytes(this.pbeKeySpec.getPassword());
		} else {
			return this.type == 5 ? PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(this.pbeKeySpec.getPassword())
					: PBEParametersGenerator.PKCS5PasswordToBytes(this.pbeKeySpec.getPassword());
		}
	}

	public String getFormat() {
		return "RAW";
	}

	public int getIterationCount() {
		return this.pbeKeySpec.getIterationCount();
	}

	public int getIvSize() {
		return this.ivSize;
	}

	int getKeySize() {
		return this.keySize;
	}

	public CipherParameters getParam() {
		return this.param;
	}

	public char[] getPassword() {
		return this.pbeKeySpec.getPassword();
	}

	public byte[] getSalt() {
		return this.pbeKeySpec.getSalt();
	}

	int getType() {
		return this.type;
	}

	public void setTryWrongPKCS12Zero(boolean var1) {
		this.tryWrong = var1;
	}

	boolean shouldTryWrongPKCS12() {
		return this.tryWrong;
	}
}
