package org.spongycastle.jcajce.provider.symmetric;

public final class AES {
	private static final Class gcmSpecClass = lookup("javax.crypto.spec.GCMParameterSpec");

	private AES() {
	}

	private static Class lookup(String var0) {
		try {
			Class var2 = AES.class.getClassLoader().loadClass(var0);
			return var2;
		} catch (Exception var3) {
			return null;
		}
	}
}
