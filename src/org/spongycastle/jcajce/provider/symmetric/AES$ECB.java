package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BlockCipherProvider;

public class AES$ECB extends BaseBlockCipher {
	public AES$ECB() {
		super((BlockCipherProvider) (new AES$ECB$1()));
	}
}
