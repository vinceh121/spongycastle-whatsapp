package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.BufferedBlockCipher;
import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.CFBBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$CFB extends BaseBlockCipher {
	public AES$CFB() {
		super((BufferedBlockCipher) (new BufferedBlockCipher(new CFBBlockCipher(new AESFastEngine(), 128))), 128);
	}
}
