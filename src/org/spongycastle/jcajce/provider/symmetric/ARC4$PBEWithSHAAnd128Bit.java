package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.engines.RC4Engine;
import org.spongycastle.jcajce.provider.symmetric.util.BaseStreamCipher;

public class ARC4$PBEWithSHAAnd128Bit extends BaseStreamCipher {
	public ARC4$PBEWithSHAAnd128Bit() {
		super(new RC4Engine(), 0, 128, 1);
	}
}
