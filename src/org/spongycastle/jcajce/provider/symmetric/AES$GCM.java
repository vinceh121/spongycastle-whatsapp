package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.AEADBlockCipher;
import org.spongycastle.crypto.modes.GCMBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$GCM extends BaseBlockCipher {
	public AES$GCM() {
		super((AEADBlockCipher) (new GCMBlockCipher(new AESFastEngine())));
	}
}
