package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA224Digest;

public class SHA224$Digest extends BCMessageDigest implements Cloneable {
	public SHA224$Digest() {
		super(new SHA224Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		SHA224$Digest var1 = (SHA224$Digest) super.clone();
		var1.digest = new SHA224Digest((SHA224Digest) this.digest);
		return var1;
	}
}
