package org.spongycastle.jcajce.provider.digest;

public class SHA3$Digest384 extends SHA3$DigestSHA3 {
	public SHA3$Digest384() {
		super(384);
	}
}
