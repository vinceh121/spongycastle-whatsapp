package org.spongycastle.jcajce.provider.digest;

public class SHA3$Digest256 extends SHA3$DigestSHA3 {
	public SHA3$Digest256() {
		super(256);
	}
}
