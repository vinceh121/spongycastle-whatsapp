package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA256$KeyGenerator extends BaseKeyGenerator {
	public SHA256$KeyGenerator() {
		super("HMACSHA256", 256, new CipherKeyGenerator());
	}
}
