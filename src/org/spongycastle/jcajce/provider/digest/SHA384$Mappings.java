package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA384$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA384.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA-384", PREFIX + "$Digest");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA384", "SHA-384");
		var1.addAlgorithm("Mac.OLDHMACSHA384", PREFIX + "$OldSHA384");
		this.addHMACAlgorithm(var1, "SHA384", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
	}
}
