package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA1$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA1.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA-1", PREFIX + "$Digest");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA1", "SHA-1");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA", "SHA-1");
		this.addHMACAlgorithm(var1, "SHA1", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
		var1.addAlgorithm("Mac.PBEWITHHMACSHA", PREFIX + "$SHA1Mac");
		var1.addAlgorithm("Mac.PBEWITHHMACSHA1", PREFIX + "$SHA1Mac");
		var1.addAlgorithm("Alg.Alias.SecretKeyFactory.PBEWITHHMACSHA", "PBEWITHHMACSHA1");
		var1.addAlgorithm("SecretKeyFactory.PBEWITHHMACSHA1", PREFIX + "$PBEWithMacKeyFactory");
		var1.addAlgorithm("SecretKeyFactory.PBKDF2WithHmacSHA1", PREFIX + "$PBKDF2WithHmacSHA1UTF8");
		var1.addAlgorithm("Alg.Alias.SecretKeyFactory.PBKDF2WithHmacSHA1AndUTF8", "PBKDF2WithHmacSHA1");
		var1.addAlgorithm("SecretKeyFactory.PBKDF2WithHmacSHA1And8BIT", PREFIX + "$PBKDF2WithHmacSHA18BIT");
		var1.addAlgorithm("Alg.Alias.SecretKeyFactory.PBKDF2withASCII", "PBKDF2WithHmacSHA1And8BIT");
		var1.addAlgorithm("Alg.Alias.SecretKeyFactory.PBKDF2with8BIT", "PBKDF2WithHmacSHA1And8BIT");
	}
}
