package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA512tDigest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.jcajce.provider.symmetric.util.BaseMac;

public class SHA512$HashMacT256 extends BaseMac {
	public SHA512$HashMacT256() {
		super(new HMac(new SHA512tDigest(256)));
	}
}
