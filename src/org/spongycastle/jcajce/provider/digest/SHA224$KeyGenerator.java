package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA224$KeyGenerator extends BaseKeyGenerator {
	public SHA224$KeyGenerator() {
		super("HMACSHA224", 224, new CipherKeyGenerator());
	}
}
