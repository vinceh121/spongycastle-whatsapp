package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.MD5Digest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.jcajce.provider.symmetric.util.BaseMac;

public class MD5$HashMac extends BaseMac {
	public MD5$HashMac() {
		super(new HMac(new MD5Digest()));
	}
}
