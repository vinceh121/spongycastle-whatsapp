package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA1Digest;

public class SHA1$Digest extends BCMessageDigest implements Cloneable {
	public SHA1$Digest() {
		super(new SHA1Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		SHA1$Digest var1 = (SHA1$Digest) super.clone();
		var1.digest = new SHA1Digest((SHA1Digest) this.digest);
		return var1;
	}
}
