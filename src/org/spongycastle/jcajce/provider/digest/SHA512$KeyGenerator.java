package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA512$KeyGenerator extends BaseKeyGenerator {
	public SHA512$KeyGenerator() {
		super("HMACSHA512", 512, new CipherKeyGenerator());
	}
}
